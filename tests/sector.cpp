/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This implements tests for the sector class
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <fatpp/sector.hpp>

SCENARIO("Sector object must be constructable", "[types]")
{
    GIVEN("A sector containing a small buffer and no address")
    {
        std::byte     buffer;
        fatpp::Sector sector{&buffer, sizeof(buffer)};
        THEN("It should be the correct size") { REQUIRE(sector.size() == sizeof(buffer)); }
        THEN("It should be clean") { REQUIRE(!sector.isDirty()); }
        THEN("It should be not have an address") { REQUIRE(!sector.hasAddress()); }
        THEN("The begin function should return the buffer") { REQUIRE(sector.begin() == &buffer); }
        THEN("The data function should return the buffer") { REQUIRE(sector.data() == &buffer); }
        THEN("The end function should return past the buffer")
        {
            REQUIRE(sector.end() == &buffer + 1);
        }
    }
    GIVEN("A sector containing a small buffer and a address")
    {
        std::byte     buffer;
        fatpp::Sector sector{fatpp::SectorAddress{100}, &buffer, sizeof(buffer)};
        THEN("It should be the correct size") { REQUIRE(sector.size() == sizeof(buffer)); }
        THEN("It should be clean") { REQUIRE(!sector.isDirty()); }
        THEN("It should be not have an address") { REQUIRE(sector.hasAddress()); }
        THEN("The address should be correct")
        {
            REQUIRE(sector.address() == fatpp::SectorAddress{100});
        }
        THEN("The begin function should return the buffer") { REQUIRE(sector.begin() == &buffer); }
        THEN("The data function should return the buffer") { REQUIRE(sector.data() == &buffer); }
        THEN("The end function should return past the buffer")
        {
            REQUIRE(sector.end() == &buffer + 1);
        }
    }
}

SCENARIO("Sector can contain addresses", "[types]")
{
    GIVEN("A sector containing a small buffer and no address")
    {
        std::byte     buffer;
        fatpp::Sector sector{&buffer, sizeof(buffer)};
        THEN("It should have no address")
        {
            REQUIRE(sector.numAddresses() == 0);
            REQUIRE(!sector.hasAddress());
        }
        THEN("Adding an address should make it assigned")
        {
            sector.addAddress(fatpp::SectorAddress{0x100});
            REQUIRE(sector.hasAddress());
            REQUIRE(sector.address() == fatpp::SectorAddress{0x100});
            REQUIRE(sector.numAddresses() == 1);
            THEN("Adding a second address should increase the number of addresses to 2")
            {
                sector.addAddress(fatpp::SectorAddress{0x105});
                REQUIRE(sector.hasAddress());
                REQUIRE(sector.address(0) == fatpp::SectorAddress{0x100});
                REQUIRE(sector.address(1) == fatpp::SectorAddress{0x105});
                REQUIRE(sector.numAddresses() == 2);
            }
        }
    }
    GIVEN("A sector containing a small buffer and a address")
    {
        std::byte     buffer;
        fatpp::Sector sector{fatpp::SectorAddress{50}, &buffer, sizeof(buffer)};
        THEN("We should be able to add an address")
        {
            REQUIRE(sector.hasAddress());
            REQUIRE(sector.numAddresses() == 1);
            REQUIRE(sector.address() == fatpp::SectorAddress{50});
        }
        THEN("The addresses function should return the vector")
        {
            auto &v = sector.addresses();
            REQUIRE(v.size() == 1);
            REQUIRE(v[0] == fatpp::SectorAddress{50});
        }
    }
}