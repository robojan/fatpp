/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file tests the FAT interface
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <mock/fat.hpp>
#include <fatpp/detail/fat.hpp>
#include <algorithm>

using namespace fatpp;

SCENARIO("MockFat needs to be a good mock for the FAT system", "[fat]")
{
    GIVEN("An empty MockFat12")
    {
        MockFat12 mock({SectorAddress{10}}, SectorCount{1}, 512);
        REQUIRE(mock.locations().size() == 1);
        auto &fat          = mock.getFat(0);
        auto  isRangeValue = [&](size_t begin, size_t end = 512, uint8_t val = 0)
        {
            return std::all_of(fat.begin() + begin, fat.begin() + end,
                               [&](std::byte x) { return x == std::byte{val}; });
        };
        THEN("The fat should be all 0") { REQUIRE(isRangeValue(0, 512, 0)); }
        THEN("Read should return the correct FAT")
        {
            auto result = mock.read(SectorAddress{10});
            REQUIRE(result);
            auto &sector = result.result();
            REQUIRE(sector->hasAddress());
            REQUIRE(sector->address() == SectorAddress{10});
            REQUIRE(std::equal(fat.begin(), fat.end(), sector->data()));
            THEN("Modifying and marking as dirty should write the changes.")
            {
                sector->data()[0] = std::byte{0xaa};
                sector->data()[1] = std::byte{0xab};
                sector->data()[2] = std::byte{0xac};
                sector->markDirty();
                // Release the sector
                sector.reset();
                REQUIRE(isRangeValue(3, 512, 0));
                REQUIRE(fat[0] == std::byte{0xaa});
                REQUIRE(fat[1] == std::byte{0xab});
                REQUIRE(fat[2] == std::byte{0xac});
            }
        }
        GIVEN("The first FAT is set to 0x123")
        {
            mock.set(ClusterAddress{0x123}, 0U, 0);
            THEN("Byte 0 and 1 should be changed")
            {
                REQUIRE(isRangeValue(2, 512, 0));
                REQUIRE(fat[0] == std::byte{0x23});
                REQUIRE(fat[1] == std::byte{0x01});
                REQUIRE(mock.get(0U) == ClusterAddress{0x123});
            }
            GIVEN("The second FAT is set to 0x456")
            {
                mock.set(ClusterAddress{0x456}, 1U, 0);
                THEN("Byte 0, 1 and 2 should be changed")
                {
                    REQUIRE(isRangeValue(3, 512, 0));
                    REQUIRE(fat[0] == std::byte{0x23});
                    REQUIRE(fat[1] == std::byte{0x61});
                    REQUIRE(fat[2] == std::byte{0x45});
                    REQUIRE(mock.get(0U) == ClusterAddress{0x123});
                    REQUIRE(mock.get(1U) == ClusterAddress{0x456});
                }
                THEN("Read should return the correct FAT")
                {
                    auto result = mock.read(SectorAddress{10});
                    REQUIRE(result);
                    auto &sector = result.result();
                    REQUIRE(sector->hasAddress());
                    REQUIRE(sector->address() == SectorAddress{10});
                    REQUIRE(std::equal(fat.begin(), fat.end(), sector->data()));
                }
            }
        }
        GIVEN("The second FAT is set to 0x123")
        {
            mock.set(ClusterAddress{0x123}, 1U, 0);
            THEN("Byte 1 and 2 should be changed")
            {
                REQUIRE(isRangeValue(3, 512, 0));
                REQUIRE(fat[0] == std::byte{0x00});
                REQUIRE(fat[1] == std::byte{0x30});
                REQUIRE(fat[2] == std::byte{0x12});
                REQUIRE(mock.get(1U) == ClusterAddress{0x123});
            }
        }
    }


    GIVEN("An empty MockFat16")
    {
        MockFat16 mock({SectorAddress{10}}, SectorCount{1}, 512);
        REQUIRE(mock.locations().size() == 1);
        auto &fat          = mock.getFat(0);
        auto  isRangeValue = [&](size_t begin, size_t end = 512, uint8_t val = 0)
        {
            return std::all_of(fat.begin() + begin, fat.begin() + end,
                               [&](std::byte x) { return x == std::byte{val}; });
        };
        THEN("The fat should be all 0") { REQUIRE(isRangeValue(0, 512, 0)); }
        THEN("Read should return the correct FAT")
        {
            auto result = mock.read(SectorAddress{10});
            REQUIRE(result);
            auto &sector = result.result();
            REQUIRE(sector->hasAddress());
            REQUIRE(sector->address() == SectorAddress{10});
            REQUIRE(std::equal(fat.begin(), fat.end(), sector->data()));
            THEN("Modifying and marking as dirty should write the changes.")
            {
                sector->data()[0] = std::byte{0xaa};
                sector->data()[1] = std::byte{0xab};
                sector->data()[2] = std::byte{0xac};
                sector->markDirty();
                // Release the sector
                sector.reset();
                REQUIRE(isRangeValue(3, 512, 0));
                REQUIRE(fat[0] == std::byte{0xaa});
                REQUIRE(fat[1] == std::byte{0xab});
                REQUIRE(fat[2] == std::byte{0xac});
            }
        }
        GIVEN("The first FAT is set to 0x1234")
        {
            mock.set(ClusterAddress{0x1234}, 0U, 0);
            THEN("Byte 0 and 1 should be changed")
            {
                REQUIRE(isRangeValue(2, 512, 0));
                REQUIRE(fat[0] == std::byte{0x34});
                REQUIRE(fat[1] == std::byte{0x12});
                REQUIRE(mock.get(0U) == ClusterAddress{0x1234});
            }
            GIVEN("The second FAT is set to 0x5678")
            {
                mock.set(ClusterAddress{0x5678}, 1U, 0);
                THEN("Byte 2 and 3 should be changed")
                {
                    REQUIRE(isRangeValue(4, 512, 0));
                    REQUIRE(fat[0] == std::byte{0x34});
                    REQUIRE(fat[1] == std::byte{0x12});
                    REQUIRE(fat[2] == std::byte{0x78});
                    REQUIRE(fat[3] == std::byte{0x56});
                    REQUIRE(mock.get(0U) == ClusterAddress{0x1234});
                    REQUIRE(mock.get(1U) == ClusterAddress{0x5678});
                }
                THEN("Read should return the correct FAT")
                {
                    auto result = mock.read(SectorAddress{10});
                    REQUIRE(result);
                    auto &sector = result.result();
                    REQUIRE(sector->hasAddress());
                    REQUIRE(sector->address() == SectorAddress{10});
                    REQUIRE(std::equal(fat.begin(), fat.end(), sector->data()));
                }
            }
        }
        GIVEN("The second FAT is set to 0x1234")
        {
            mock.set(ClusterAddress{0x1234}, 1U, 0);
            THEN("Byte 2 and 3 should be changed")
            {
                REQUIRE(isRangeValue(4, 512, 0));
                REQUIRE(fat[0] == std::byte{0x00});
                REQUIRE(fat[1] == std::byte{0x00});
                REQUIRE(fat[2] == std::byte{0x34});
                REQUIRE(fat[3] == std::byte{0x12});
                REQUIRE(mock.get(1U) == ClusterAddress{0x1234});
            }
        }
    }

    GIVEN("An empty MockFat32")
    {
        MockFat32 mock({SectorAddress{10}}, SectorCount{1}, 512);
        REQUIRE(mock.locations().size() == 1);
        auto &fat          = mock.getFat(0);
        auto  isRangeValue = [&](size_t begin, size_t end = 512, uint8_t val = 0)
        {
            return std::all_of(fat.begin() + begin, fat.begin() + end,
                               [&](std::byte x) { return x == std::byte{val}; });
        };
        THEN("The fat should be all 0") { REQUIRE(isRangeValue(0, 512, 0)); }
        THEN("Read should return the correct FAT")
        {
            auto result = mock.read(SectorAddress{10});
            REQUIRE(result);
            auto &sector = result.result();
            REQUIRE(sector->hasAddress());
            REQUIRE(sector->address() == SectorAddress{10});
            REQUIRE(std::equal(fat.begin(), fat.end(), sector->data()));
            THEN("Modifying and marking as dirty should write the changes.")
            {
                sector->data()[0] = std::byte{0xaa};
                sector->data()[1] = std::byte{0xab};
                sector->data()[2] = std::byte{0xac};
                sector->markDirty();
                // Release the sector
                sector.reset();
                REQUIRE(isRangeValue(3, 512, 0));
                REQUIRE(fat[0] == std::byte{0xaa});
                REQUIRE(fat[1] == std::byte{0xab});
                REQUIRE(fat[2] == std::byte{0xac});
            }
        }
        GIVEN("The first FAT is set to 0x01234567")
        {
            mock.set(ClusterAddress{0x01234567}, 0U, 0);
            THEN("Byte 0 to 3 should be changed")
            {
                REQUIRE(isRangeValue(4, 512, 0));
                REQUIRE(fat[0] == std::byte{0x67});
                REQUIRE(fat[1] == std::byte{0x45});
                REQUIRE(fat[2] == std::byte{0x23});
                REQUIRE(fat[3] == std::byte{0x01});
                REQUIRE(mock.get(0U) == ClusterAddress{0x01234567});
            }
            GIVEN("The second FAT is set to 0x89abcdef")
            {
                mock.set(ClusterAddress{0x89abcdef}, 1U, 0);
                THEN("Byte 0 to 7 should be changed")
                {
                    REQUIRE(isRangeValue(8, 512, 0));
                    REQUIRE(fat[0] == std::byte{0x67});
                    REQUIRE(fat[1] == std::byte{0x45});
                    REQUIRE(fat[2] == std::byte{0x23});
                    REQUIRE(fat[3] == std::byte{0x01});
                    REQUIRE(fat[4] == std::byte{0xef});
                    REQUIRE(fat[5] == std::byte{0xcd});
                    REQUIRE(fat[6] == std::byte{0xab});
                    REQUIRE(fat[7] == std::byte{0x89});
                    REQUIRE(mock.get(0U) == ClusterAddress{0x01234567});
                    REQUIRE(mock.get(1U) == ClusterAddress{0x89abcdef});
                }
                THEN("Read should return the correct FAT")
                {
                    auto result = mock.read(SectorAddress{10});
                    REQUIRE(result);
                    auto &sector = result.result();
                    REQUIRE(sector->hasAddress());
                    REQUIRE(sector->address() == SectorAddress{10});
                    REQUIRE(std::equal(fat.begin(), fat.end(), sector->data()));
                }
            }
        }
        GIVEN("The second FAT is set to 0x01234567")
        {
            mock.set(ClusterAddress{0x01234567}, 1U, 0);
            THEN("Byte 4 to 7 should be changed")
            {
                REQUIRE(isRangeValue(8, 512, 0));
                REQUIRE(fat[0] == std::byte{0x00});
                REQUIRE(fat[1] == std::byte{0x00});
                REQUIRE(fat[2] == std::byte{0x00});
                REQUIRE(fat[3] == std::byte{0x00});
                REQUIRE(fat[4] == std::byte{0x67});
                REQUIRE(fat[5] == std::byte{0x45});
                REQUIRE(fat[6] == std::byte{0x23});
                REQUIRE(fat[7] == std::byte{0x01});
                REQUIRE(mock.get(1U) == ClusterAddress{0x01234567});
            }
        }
    }
}


SCENARIO("FAT12 should work", "[fat]")
{
    GIVEN("A MockFat12 with sectorsize of 512 with 1300 fs entries")
    {
        MockFat12 mock({SectorAddress{10}, SectorAddress{20}}, SectorCount{4}, 512);
        auto     &rawFat          = mock.getFat(0);
        auto      isValidAndValue = [](Result<ClusterAddress> addr, unsigned val)
        { return addr && addr->get() == val; };

        fatpp::detail::State fsState{
            .disk              = mock,
            .fatLocations      = {SectorAddress{10}, SectorAddress{20}},
            .fatSize           = SectorCount{4},
            .sectorsPerCluster = SectorCount{1},
            .firstDataSector   = SectorAddress{100},
            .rootDirEntries    = DirEntryCount{0},
            .sectorSize        = 9,
            .sectorMask        = 511,
            .fsSize            = ClusterCount{1300},
            .lastFreeCluster   = ClusterAddress{0},
        };

        fatpp::detail::Fat12 fat{fsState};

        THEN("Get Free cluster should give cluster 2")
        {
            REQUIRE(isValidAndValue(fat.getFreeCluster(), 2));
        }
        GIVEN("The last free cluster is 50")
        {
            fsState.lastFreeCluster = ClusterAddress{50};
            THEN("Get free cluster should give cluster 50")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
            }
        }
        GIVEN("The last free cluster is 341")
        {
            fsState.lastFreeCluster = ClusterAddress{341};
            THEN("Get free cluster should give cluster 341")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 341));
            }
            GIVEN("Cluster 341 is not empty")
            {
                mock.set(ClusterAddress{0x123}, 341);
                THEN("Get free cluster should give cluster 342")
                {
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 342));
                }
            }
        }
        GIVEN("The last free cluster is 1299")
        {
            fsState.lastFreeCluster = ClusterAddress{1299};
            THEN("Get free cluster should give cluster 1299")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 1299));
            }
            GIVEN("Cluster 1299 is not empty")
            {
                mock.set(ClusterAddress{0x123}, 1299);
                THEN("Get free cluster should give cluster 2")
                {
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 2));
                }
                GIVEN("Cluster 2 is not empty")
                {
                    mock.set(ClusterAddress{0x123}, 2);
                    THEN("Get free cluster should give cluster 3")
                    {
                        REQUIRE(isValidAndValue(fat.getFreeCluster(), 3));
                    }
                }
            }
        }
        GIVEN("A full filesystem")
        {
            for(auto i = 0; i < fsState.fsSize.get(); i++)
            {
                mock.set(fat.kEofCluster, i);
            }
            THEN("Get Free cluster should return no space")
            {
                auto result = fat.getFreeCluster();
                REQUIRE(result.status() == ErrorCode::NoSpace);
            }
            GIVEN("Cluster 50 is cleared")
            {
                mock.set(ClusterAddress{0}, 50);
                THEN("Get Free cluster should give cluster 50")
                {
                    auto result = fat.getFreeCluster();
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
                }
                GIVEN("The last free cluster is 51")
                {
                    fsState.lastFreeCluster = ClusterAddress{51};
                    THEN("Get free cluster should give cluster 50")
                    {
                        REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
                    }
                }
            }
        }
        GIVEN("An entry at pos 9")
        {
            mock.set(ClusterAddress{0x123}, 9);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{8}), 0));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{9}), 0x123));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{10}), 0));
            }
        }
        GIVEN("An entry at pos 10")
        {
            mock.set(ClusterAddress{0x123}, 10);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{8}), 0));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{10}), 0x123));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{11}), 0));
            }
        }
        GIVEN("An entry at pos 1299")
        {
            mock.set(ClusterAddress{0x123}, 1299);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{1299}), 0x123));
            }
        }
        GIVEN("An entry at the cross over pos 341")
        {
            mock.set(ClusterAddress{0x123}, 341);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{341}), 0x123));
            }
        }
        THEN("Setting sector 5 should work")
        {
            REQUIRE(fat.setCluster(ClusterAddress{5}, ClusterAddress{0x321}));
            REQUIRE(mock.get(5, 0) == ClusterAddress{0x321});
            REQUIRE(mock.get(5, 1) == ClusterAddress{0x321});
        }
    }
}


SCENARIO("FAT16 should work", "[fat]")
{
    GIVEN("A MockFat16 with sectorsize of 512 with 1300 fs entries")
    {
        MockFat16 mock({SectorAddress{10}, SectorAddress{20}}, SectorCount{4}, 512);
        auto     &rawFat          = mock.getFat(0);
        auto      isValidAndValue = [](Result<ClusterAddress> addr, unsigned val)
        { return addr && addr->get() == val; };

        fatpp::detail::State fsState{
            .disk              = mock,
            .fatLocations      = {SectorAddress{10}, SectorAddress{20}},
            .fatSize           = SectorCount{4},
            .sectorsPerCluster = SectorCount{1},
            .firstDataSector   = SectorAddress{100},
            .rootDirEntries    = DirEntryCount{0},
            .sectorSize        = 9,
            .sectorMask        = 511,
            .fsSize            = ClusterCount{1000},
            .lastFreeCluster   = ClusterAddress{0},
        };

        fatpp::detail::Fat16 fat{fsState};

        THEN("Get Free cluster should give cluster 2")
        {
            REQUIRE(isValidAndValue(fat.getFreeCluster(), 2));
        }
        GIVEN("The last free cluster is 50")
        {
            fsState.lastFreeCluster = ClusterAddress{50};
            THEN("Get free cluster should give cluster 50")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
            }
        }
        GIVEN("The last free cluster is 999")
        {
            fsState.lastFreeCluster = ClusterAddress{999};
            THEN("Get free cluster should give cluster 999")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 999));
            }
            GIVEN("Cluster 999 is not empty")
            {
                mock.set(ClusterAddress{0x1234}, 999);
                THEN("Get free cluster should give cluster 2")
                {
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 2));
                }
                GIVEN("Cluster 2 is not empty")
                {
                    mock.set(ClusterAddress{0x1234}, 2);
                    THEN("Get free cluster should give cluster 3")
                    {
                        REQUIRE(isValidAndValue(fat.getFreeCluster(), 3));
                    }
                }
            }
        }
        GIVEN("A full filesystem")
        {
            for(auto i = 0; i < fsState.fsSize.get(); i++)
            {
                mock.set(fat.kEofCluster, i);
            }
            THEN("Get Free cluster should return no space")
            {
                auto result = fat.getFreeCluster();
                REQUIRE(result.status() == ErrorCode::NoSpace);
            }
            GIVEN("Cluster 50 is cleared")
            {
                mock.set(ClusterAddress{0}, 50);
                THEN("Get Free cluster should give cluster 50")
                {
                    auto result = fat.getFreeCluster();
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
                }
                GIVEN("The last free cluster is 51")
                {
                    fsState.lastFreeCluster = ClusterAddress{51};
                    THEN("Get free cluster should give cluster 50")
                    {
                        REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
                    }
                }
            }
        }
        GIVEN("An entry at pos 9")
        {
            mock.set(ClusterAddress{0x1234}, 9);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{8}), 0));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{9}), 0x1234));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{10}), 0));
            }
        }
        GIVEN("An entry at pos 10")
        {
            mock.set(ClusterAddress{0x1234}, 10);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{8}), 0));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{10}), 0x1234));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{11}), 0));
            }
        }
        GIVEN("An entry at pos 999")
        {
            mock.set(ClusterAddress{0x1234}, 999);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{999}), 0x1234));
            }
        }
        THEN("Setting sector 5 should work")
        {
            REQUIRE(fat.setCluster(ClusterAddress{5}, ClusterAddress{0x4321}));
            REQUIRE(mock.get(5, 0) == ClusterAddress{0x4321});
            REQUIRE(mock.get(5, 1) == ClusterAddress{0x4321});
        }
    }
}

SCENARIO("FAT32 should work", "[fat]")
{
    GIVEN("A MockFat32 with sectorsize of 512 with 1300 fs entries")
    {
        MockFat32 mock({SectorAddress{10}, SectorAddress{20}}, SectorCount{4}, 512);
        auto     &rawFat          = mock.getFat(0);
        auto      isValidAndValue = [](Result<ClusterAddress> addr, unsigned val)
        { return addr && addr->get() == val; };

        fatpp::detail::State fsState{
            .disk              = mock,
            .fatLocations      = {SectorAddress{10}, SectorAddress{20}},
            .fatSize           = SectorCount{4},
            .sectorsPerCluster = SectorCount{1},
            .firstDataSector   = SectorAddress{100},
            .rootDirEntries    = DirEntryCount{0},
            .sectorSize        = 9,
            .sectorMask        = 511,
            .fsSize            = ClusterCount{500},
            .lastFreeCluster   = ClusterAddress{0},
        };

        fatpp::detail::Fat32 fat{fsState};

        THEN("Get Free cluster should give cluster 2")
        {
            REQUIRE(isValidAndValue(fat.getFreeCluster(), 2));
        }
        GIVEN("The last free cluster is 50")
        {
            fsState.lastFreeCluster = ClusterAddress{50};
            THEN("Get free cluster should give cluster 50")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
            }
        }
        GIVEN("The last free cluster is 499")
        {
            fsState.lastFreeCluster = ClusterAddress{499};
            THEN("Get free cluster should give cluster 499")
            {
                REQUIRE(isValidAndValue(fat.getFreeCluster(), 499));
            }
            GIVEN("Cluster 499 is not empty")
            {
                mock.set(ClusterAddress{0x12345678}, 499);
                THEN("Get free cluster should give cluster 2")
                {
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 2));
                }
                GIVEN("Cluster 2 is not empty")
                {
                    mock.set(ClusterAddress{0x12345678}, 2);
                    THEN("Get free cluster should give cluster 3")
                    {
                        REQUIRE(isValidAndValue(fat.getFreeCluster(), 3));
                    }
                }
            }
        }
        GIVEN("A full filesystem")
        {
            for(auto i = 0; i < fsState.fsSize.get(); i++)
            {
                mock.set(fat.kEofCluster, i);
            }
            THEN("Get Free cluster should return no space")
            {
                auto result = fat.getFreeCluster();
                REQUIRE(result.status() == ErrorCode::NoSpace);
            }
            GIVEN("Cluster 50 is cleared")
            {
                mock.set(ClusterAddress{0}, 50);
                THEN("Get Free cluster should give cluster 50")
                {
                    auto result = fat.getFreeCluster();
                    REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
                }
                GIVEN("The last free cluster is 51")
                {
                    fsState.lastFreeCluster = ClusterAddress{51};
                    THEN("Get free cluster should give cluster 50")
                    {
                        REQUIRE(isValidAndValue(fat.getFreeCluster(), 50));
                    }
                }
            }
        }
        GIVEN("An entry at pos 9")
        {
            mock.set(ClusterAddress{0x12345678}, 9);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{8}), 0));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{9}), 0x2345678));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{10}), 0));
            }
        }
        GIVEN("An entry at pos 10")
        {
            mock.set(ClusterAddress{0x12345678}, 10);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{8}), 0));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{10}), 0x2345678));
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{11}), 0));
            }
        }
        GIVEN("An entry at pos 499")
        {
            mock.set(ClusterAddress{0x12345678}, 499);
            THEN("Getting the next cluster should succeed")
            {
                REQUIRE(isValidAndValue(fat.getNextCluster(ClusterAddress{499}), 0x2345678));
            }
        }
        THEN("Setting sector 5 should work")
        {
            REQUIRE(fat.setCluster(ClusterAddress{5}, ClusterAddress{0x07654321}));
            REQUIRE(mock.get(5, 0) == ClusterAddress{0x07654321});
            REQUIRE(mock.get(5, 1) == ClusterAddress{0x07654321});
        }
        GIVEN("The top 4 bits are set of cluster 6")
        {
            mock.set(ClusterAddress{0xaeeeeeee}, 6);

            THEN("Setting sector 6 should work, and keep them intact")
            {
                REQUIRE(fat.setCluster(ClusterAddress{6}, ClusterAddress{0x07654321}));
                REQUIRE(mock.get(6, 0) == ClusterAddress{0xa7654321});
                REQUIRE(mock.get(6, 1) == ClusterAddress{0xa7654321});
            }
        }
    }
}

SCENARIO("The FAT iterator can follow FAT chains")
{
    GIVEN("A FAT12 system")
    {
        MockFat32 mock({SectorAddress{10}}, SectorCount{4}, 512);
        auto     &rawFat          = mock.getFat(0);
        auto      isValidAndValue = [](Result<ClusterAddress> addr, unsigned val)
        { return addr && addr->get() == val; };

        fatpp::detail::State fsState{
            .disk              = mock,
            .fatLocations      = {SectorAddress{10}},
            .fatSize           = SectorCount{4},
            .sectorsPerCluster = SectorCount{1},
            .firstDataSector   = SectorAddress{100},
            .rootDirEntries    = DirEntryCount{0},
            .sectorSize        = 9,
            .sectorMask        = 511,
            .fsSize            = ClusterCount{500},
            .lastFreeCluster   = ClusterAddress{0},
        };

        fatpp::detail::Fat32 fat{fsState};
        AND_GIVEN("A FAT Chain 5 -> 10")
        {
            mock.set(ClusterAddress{10}, 5);
            mock.set(fat.kEofCluster, 10);
            THEN("Getting a chain at address 5 should work")
            {
                auto it = fat.getChain(ClusterAddress{5});
                REQUIRE(it);
                REQUIRE(!it.isLast());
                REQUIRE(*it == ClusterAddress{5});
                AND_THEN("++it should work")
                {
                    auto val = ++it;
                    REQUIRE(it);
                    REQUIRE(val);
                    REQUIRE(!it.isLast());
                    REQUIRE(!val.isLast());
                    REQUIRE(it == val);
                    REQUIRE(*val == ClusterAddress{10});
                    REQUIRE(*it == ClusterAddress{10});
                    AND_THEN("++it should work")
                    {
                        auto val = ++it;
                        REQUIRE(!it);
                        REQUIRE(!val);
                        REQUIRE(it == val);
                        REQUIRE(it.isLast());
                        REQUIRE(val.isLast());
                        REQUIRE(*val == fat.kEofCluster);
                        REQUIRE(*it == fat.kEofCluster);
                    }
                    AND_THEN("it++ should work")
                    {
                        auto val = it++;
                        REQUIRE(!it);
                        REQUIRE(val);
                        REQUIRE(it != val);
                        REQUIRE(it.isLast());
                        REQUIRE(!val.isLast());
                        REQUIRE(*val == ClusterAddress{10});
                        REQUIRE(*it == fat.kEofCluster);
                    }
                }
                AND_THEN("it++ should work")
                {
                    auto val = it++;
                    REQUIRE(it);
                    REQUIRE(val);
                    REQUIRE(it != val);
                    REQUIRE(!it.isLast());
                    REQUIRE(!val.isLast());
                    REQUIRE(*val == ClusterAddress{5});
                    REQUIRE(*it == ClusterAddress{10});
                }
            }
        }
    }
}