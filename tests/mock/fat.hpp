/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This files contains a implementation of the IDisk interface to test the FAT
 * component of the library.
 ****************************************************************************/
#pragma once

#include <initializer_list>
#include <vector>
#include <cstddef>
#include <memory>

#include <fatpp/disk.hpp>
#include <fatpp/detail/types.hpp>
#include <fatpp/sector.hpp>

namespace fatpp
{

class MockFat : public IDisk
{
public:
    MockFat(const std::initializer_list<SectorAddress> &locations,
            SectorCount                                 fatSize,
            unsigned                                    sectorSize);

    std::shared_ptr<Sector> getBuffer() override;

    SimpleResult                    write(std::shared_ptr<Sector> &data) override;
    Result<std::shared_ptr<Sector>> read(SectorAddress addr) override;

    inline SectorCount size() const override
    {
        return SectorCount{_fatSize.get() * _locations.size() + 10};
    }

    virtual ClusterAddress get(unsigned idx, unsigned fatNr = 0) const = 0;
    void                   set(ClusterAddress val, unsigned idx);
    virtual void           set(ClusterAddress val, unsigned idx, unsigned fatNr) = 0;

    std::vector<std::byte>       &getFat(unsigned idx);
    const std::vector<std::byte> &getFat(unsigned idx) const;

    const auto &locations() const { return _locations; }

protected:
    std::vector<SectorAddress>          _locations;
    SectorCount                         _fatSize;
    std::size_t                         _sectorSize;
    std::vector<std::vector<std::byte>> _fats;

    /*****************************************************************************
     * get FAT index and offset function will transform a sector address to the
     * index in the _fats variable and the byte offset from the start of the FAT.
     *
     * @param addr Sector address to transform
     * @return Error on invalid argument, else the fat index and byte offset will be returned.
     ****************************************************************************/
    Result<std::pair<unsigned, unsigned>> getFatIndexAndOffset(SectorAddress addr) const;

private:
    SimpleResult write(Sector &data);
};

template <unsigned FatWidth>
class MockFatN : public MockFat
{
public:
    MockFatN(const std::initializer_list<SectorAddress> &locations,
             SectorCount                                 fatSize,
             unsigned                                    sectorSize)
        : MockFat(locations, fatSize, sectorSize)
    {
    }

    ClusterAddress get(unsigned idx, unsigned fatNr = 0) const override
    {
        auto &fat        = getFat(fatNr);
        auto  bitIdx     = (idx * FatWidth);
        auto  byteIdx    = bitIdx / 8U;
        auto  byteOffset = bitIdx % 8U;
        auto  bytesize   = (FatWidth + 7U) / 8U;

        ClusterAddress::base_type rawValue = 0;
        ClusterAddress::base_type rawMask  = (~0U << (32U - FatWidth)) >> (32U - FatWidth);
        for(unsigned i = 0; i < bytesize; i++)
        {
            auto shiftamount = static_cast<int>(8 * i) - static_cast<int>(byteOffset);
            auto byteVal     = static_cast<uint8_t>(fat[byteIdx + i]);
            rawValue |= shiftamount < 0 ? (byteVal >> (-shiftamount)) : (byteVal << shiftamount);
        }
        return ClusterAddress{rawValue & rawMask};
    }

    using MockFat::set;

    void set(ClusterAddress val, unsigned idx, unsigned fatNr) override
    {
        auto &fat        = getFat(fatNr);
        auto  bitIdx     = (idx * FatWidth);
        auto  byteIdx    = bitIdx / 8U;
        auto  byteOffset = bitIdx % 8U;
        auto  bytesize   = (FatWidth + 7U) / 8U;

        auto                      rawValue = val.get();
        ClusterAddress::base_type rawMask  = (~0U << (32U - FatWidth)) >> (32U - FatWidth);
        for(unsigned i = 0; i < bytesize; i++)
        {
            auto shiftamount = static_cast<int>(8 * i) - static_cast<int>(byteOffset);
            auto byteVal     = static_cast<uint8_t>(fat[byteIdx + i]);
            auto mask        = ((rawMask << byteOffset) >> (8 * i)) & 0xFF;

            if(shiftamount < 0)
            {
                byteVal = ((rawValue << -shiftamount) & mask) | (byteVal & ~mask);
            }
            else
            {
                byteVal = ((rawValue >> shiftamount) & mask) | (byteVal & ~mask);
            }
            byteVal &= 0xFF;
            fat[byteIdx + i] = std::byte{byteVal};
        }
    }
};

using MockFat12 = MockFatN<12>;
using MockFat16 = MockFatN<16>;
using MockFat32 = MockFatN<32>;

} // namespace fatpp
