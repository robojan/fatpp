/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file implements the MockFat Object
 ****************************************************************************/

#include <mock/fat.hpp>
#include <memory>

using namespace fatpp;

MockFat::MockFat(const std::initializer_list<SectorAddress> &locations,
                 SectorCount                                 fatSize,
                 unsigned                                    sectorSize)
    : _locations(locations.begin(), locations.end()), _fatSize(fatSize), _sectorSize(sectorSize)
{
    _locations = std::vector<SectorAddress>(locations);
    _fats.resize(_locations.size(), std::vector<std::byte>{_fatSize.get() * sectorSize});
}

std::shared_ptr<Sector> MockFat::getBuffer()
{
    auto data    = new std::byte[_sectorSize];
    auto ptr     = new Sector{data, _sectorSize};
    auto deleter = [this](Sector *ptr)
    {
        if(ptr->isDirty())
        {
            // The sector is dirty. Write it back
            assert(ptr->hasAddress());
            write(*ptr);
        }
        delete[] ptr->data();
        delete ptr;
    };
    return std::shared_ptr<Sector>(ptr, deleter);
}

SimpleResult MockFat::write(Sector &data)
{
    // Check the data size
    if(data.size() != _sectorSize)
        return ErrorCode::InvalidArgument;

    // Write the data to all the addresses
    for(auto addr : data.addresses())
    {
        auto fatIndexAndOffset = getFatIndexAndOffset(addr);
        if(!fatIndexAndOffset)
            return fatIndexAndOffset.status();

        auto &fat  = getFat(fatIndexAndOffset->first);
        auto  dest = fat.data() + fatIndexAndOffset->second;

        std::copy(data.begin(), data.end(), dest);
    }

    // Mark the data as synced.
    data.clearDirty();

    return ErrorCode::Ok;
}

SimpleResult MockFat::write(std::shared_ptr<Sector> &data)
{
    return write(*data);
}

Result<std::shared_ptr<Sector>> MockFat::read(SectorAddress addr)
{
    auto fatIndexAndOffset = getFatIndexAndOffset(addr);
    if(!fatIndexAndOffset)
        return fatIndexAndOffset.status();

    auto buffer = getBuffer();

    buffer->addAddress(addr);

    auto &fat           = getFat(fatIndexAndOffset->first);
    auto  storageSector = std::span(fat.data() + fatIndexAndOffset->second, _sectorSize);

    std::copy(storageSector.begin(), storageSector.end(), buffer->begin());
    return buffer;
}

void MockFat::set(ClusterAddress val, unsigned idx)
{
    for(std::size_t i = 0; i < _locations.size(); ++i)
    {
        set(val, idx, i);
    }
}

std::vector<std::byte> &MockFat::getFat(unsigned idx)
{
    assert(idx < _fats.size());
    return _fats[idx];
}

const std::vector<std::byte> &MockFat::getFat(unsigned idx) const
{
    assert(idx < _fats.size());
    return _fats[idx];
}

Result<std::pair<unsigned, unsigned>> MockFat::getFatIndexAndOffset(SectorAddress addr) const
{
    for(unsigned i = 0; i < _locations.size(); ++i)
    {
        auto locAddr = _locations[i];
        if(addr >= locAddr && addr < locAddr + _fatSize)
        {
            unsigned byteOffset = (addr - locAddr).get() * _sectorSize;
            return std::make_pair(i, byteOffset);
        }
    }
    return ErrorCode::InvalidArgument;
}