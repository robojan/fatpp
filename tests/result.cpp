/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file will test the result classes
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <fatpp/result.hpp>

using namespace fatpp;

SCENARIO("SimpleResult should be constructable", "[types]")
{
    GIVEN("A default initialized SimpleResult")
    {
        SimpleResult r;
        THEN("It should be good") { REQUIRE(r); }
    }
    GIVEN("A success initialized SimpleResult")
    {
        SimpleResult r = ErrorCode::Ok;
        THEN("It should be good") { REQUIRE(r); }
        THEN("The status should be Ok") { REQUIRE(r.status() == ErrorCode::Ok); }
    }
    GIVEN("A failure initialized SimpleResult")
    {
        SimpleResult r = ErrorCode::NotImplemented;
        THEN("It should be bad") { REQUIRE(!r); }
        THEN("The status should be NotImplemented")
        {
            REQUIRE(r.status() == ErrorCode::NotImplemented);
        }
    }
}

SCENARIO("Result<int> should be constructable", "[types]")
{
    GIVEN("A default initialized Result<int>")
    {
        Result<int> r{0};
        THEN("It should be good") { REQUIRE(r); }
    }
    GIVEN("A success initialized Result<int>")
    {
        Result<int> r{125};
        THEN("It should be good") { REQUIRE(r); }
        THEN("The status should be Ok") { REQUIRE(r.status() == ErrorCode::Ok); }
        THEN("The result should be 125") { REQUIRE(*r == 125); }
        THEN("The result should be 125") { REQUIRE(r.result() == 125); }
    }
    GIVEN("A failure initialized Result<int>")
    {
        Result<int> r = {ErrorCode::NotImplemented};
        THEN("It should be bad") { REQUIRE(!r); }
        THEN("The status should be NotImplemented")
        {
            REQUIRE(r.status() == ErrorCode::NotImplemented);
        }
    }
}