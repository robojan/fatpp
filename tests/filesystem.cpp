/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains the tests for the filesystem class
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <fatpp/filesystem.hpp>

SCENARIO("The bios parameter block can be verified")
{
    using namespace fatpp::structure;
    using namespace fatpp::detail;
    GIVEN("A valid fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 1;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
    }
    GIVEN("A valid fat16 bios parameter block")
    {
        BiosParameterBlockFat16 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 1;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 10000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat16::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat16::c_sigValue;

        THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
    }
    GIVEN("A valid fat32 bios parameter block")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 0;
        bpb.totSec32   = 0x4000000;
        bpb.fatSz16    = 0;
        bpb.fatSz32    = 0x8000;
        bpb.fsVer      = 0;
        bpb.bootSig    = BiosParameterBlockFat32::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat32::c_sigValue;
        THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.secPerClus = 1;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("bytsPerSec is 0")
        {
            bpb.bytsPerSec = 0;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 256")
        {
            bpb.bytsPerSec = 256;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 512")
        {
            bpb.bytsPerSec = 512;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 700")
        {
            bpb.bytsPerSec = 700;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 1024")
        {
            bpb.bytsPerSec = 1024;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 2048")
        {
            bpb.bytsPerSec = 2048;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 4096")
        {
            bpb.bytsPerSec = 4096;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("bytsPerSec is 8192")
        {
            bpb.bytsPerSec = 8192;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.bytsPerSec = 512;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("secPerClus is 0")
        {
            bpb.secPerClus = 0;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 1")
        {
            bpb.secPerClus = 1;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 2")
        {
            bpb.secPerClus = 2;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 3")
        {
            bpb.secPerClus = 3;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 4")
        {
            bpb.secPerClus = 4;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 5")
        {
            bpb.secPerClus = 5;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 64")
        {
            bpb.secPerClus = 64;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("secPerClus is 255")
        {
            bpb.secPerClus = 63;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("with bytsPerSec is 512 and secPerClus is 64")
        {
            bpb.secPerClus = 64;
            bpb.bytsPerSec = 512;
            THEN("isValidBpb should return true") { REQUIRE(isValidBpb(bpb)); }
        }
        AND_GIVEN("with bytsPerSec is 512 and secPerClus is 128")
        {
            bpb.secPerClus = 128;
            bpb.bytsPerSec = 512;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("with bytsPerSec is 1024 and secPerClus is 64")
        {
            bpb.secPerClus = 64;
            bpb.bytsPerSec = 1024;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("rsvdSecCnt is 0")
        {
            bpb.rsvdSecCnt = 0;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("numFATs is 0")
        {
            bpb.numFATs = 0;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("totSec = 2000 and disk size = 1500")
        {
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb, 1500)); }
        }
    }
    GIVEN("A fat12 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 2000;
        bpb.fatSz16    = 6;
        bpb.bootSig    = BiosParameterBlockFat12::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat12::c_sigValue;

        AND_GIVEN("an invalid bootSig")
        {
            bpb.bootSig   = 0x12;
            bpb.signature = BiosParameterBlockFat12::c_sigValue;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("an invalid signature")
        {
            bpb.bootSig   = BiosParameterBlockFat12::c_bootSigValue;
            bpb.signature = 0x1234;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat32 bios parameter block")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 0;
        bpb.totSec32   = 0x4000000;
        bpb.fatSz16    = 0;
        bpb.fatSz32    = 0x8000;
        bpb.fsVer      = 0;
        bpb.bootSig    = BiosParameterBlockFat32::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat32::c_sigValue;

        AND_GIVEN("an invalid bootSig")
        {
            bpb.bootSig   = 0x12;
            bpb.signature = BiosParameterBlockFat32::c_sigValue;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
        AND_GIVEN("an invalid signature")
        {
            bpb.bootSig   = BiosParameterBlockFat32::c_bootSigValue;
            bpb.signature = 0x1234;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
    GIVEN("A fat32 bios parameter block")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.bytsPerSec = 512;
        bpb.secPerClus = 64;
        bpb.rsvdSecCnt = 1;
        bpb.numFATs    = 2;
        bpb.rootEntCnt = 512;
        bpb.totSec16   = 0;
        bpb.totSec32   = 0x4000000;
        bpb.fatSz16    = 0;
        bpb.fatSz32    = 0x8000;
        bpb.fsVer      = 0;
        bpb.bootSig    = BiosParameterBlockFat32::c_bootSigValue;
        bpb.signature  = BiosParameterBlockFat32::c_sigValue;

        AND_GIVEN("fs version 0.1")
        {
            bpb.fsVer = 1;
            THEN("isValidBpb should return false") { REQUIRE(!isValidBpb(bpb)); }
        }
    }
}

SCENARIO("getTotalSectors should return the total number of sectors")
{
    using namespace fatpp::structure;
    using namespace fatpp::detail;
    GIVEN("A BPB with totSec16 = 1024 and totSec32 = 0")
    {
        BiosParameterBlock bpb{.totSec16 = 1024, .totSec32 = 0};
        THEN("getTotalSectors should return totSec16") { REQUIRE(getTotalSectors(bpb) == 1024); }
    }
    GIVEN("A BPB with totSec16 = 0 and totSec32 = 1500")
    {
        BiosParameterBlock bpb{.totSec16 = 0, .totSec32 = 1500};
        THEN("getTotalSectors should return totSec32") { REQUIRE(getTotalSectors(bpb) == 1500); }
    }
    GIVEN("A BPB with totSec16 = 500 and totSec32 = 2000")
    {
        BiosParameterBlock bpb{.totSec16 = 500, .totSec32 = 2000};
        THEN("getTotalSectors should return totSec16") { REQUIRE(getTotalSectors(bpb) == 500); }
    }
}

SCENARIO("getFatSize should return the total number of sectors")
{
    using namespace fatpp::structure;
    using namespace fatpp::detail;
    GIVEN("A BPB with fatSz16 = 1024 and fatSz32 = 0")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.fatSz16 = 1024;
        bpb.fatSz32 = 0;
        THEN("getFatSize should return fatSz16") { REQUIRE(getFatSize(bpb) == 1024); }
    }
    GIVEN("A BPB with fatSz16 = 0 and fatSz32 = 1500")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.fatSz16 = 0;
        bpb.fatSz32 = 1500;
        THEN("getFatSize should return fatSz32") { REQUIRE(getFatSize(bpb) == 1500); }
    }
    GIVEN("A BPB with fatSz16 = 500 and fatSz32 = 2000")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.fatSz16 = 500;
        bpb.fatSz32 = 2000;
        THEN("getFatSize should return fatSz16") { REQUIRE(getFatSize(bpb) == 500); }
    }
    GIVEN("A BPB with fatSz16 = 500")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.fatSz16 = 500;
        THEN("getFatSize should return fatSz16") { REQUIRE(getFatSize(bpb) == 500); }
    }
    GIVEN("A BPB with fatSz16 = 0")
    {
        BiosParameterBlockFat16 bpb{};
        bpb.fatSz16 = 0;
        THEN("getFatSize should return fatSz16") { REQUIRE(getFatSize(bpb) == 0); }
    }
}

SCENARIO("The getFatType function must be able to detect the correct filesystem type.")
{
    using namespace fatpp::structure;
    using namespace fatpp::detail;
    GIVEN("A FAT12/16 bios parameter block")
    {
        BiosParameterBlockFat12 bpb{};
        bpb.rootEntCnt = 512;
        bpb.bytsPerSec = 512;
        // 32 sectors for the root dir
        bpb.fatSz16 = 16;
        bpb.numFATs = 2;
        // 16 sectors per FAT so 32 sectors for the FAT
        bpb.rsvdSecCnt = 1;
        // 65 sectors for the headers
        bpb.secPerClus = 1;

        int numReservedSectors = 65;

        AND_GIVEN("Number of data clusters 1024")
        {
            bpb.totSec16 = 1024 + numReservedSectors;
            THEN("FAT12 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT12); }
        }
        AND_GIVEN("Number of data clusters 4084")
        {
            bpb.totSec16 = 4084 + numReservedSectors;
            THEN("FAT12 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT12); }
        }
        AND_GIVEN("Number of data clusters 4085")
        {
            bpb.totSec16 = 4085 + numReservedSectors;
            THEN("FAT16 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT16); }
        }
        AND_GIVEN("Number of data clusters 32000")
        {
            bpb.totSec16 = 32000 + numReservedSectors;
            THEN("FAT16 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT16); }
        }
    }

    GIVEN("A FAT32 bios parameter block")
    {
        BiosParameterBlockFat32 bpb{};
        bpb.rootEntCnt = 0;
        bpb.bytsPerSec = 512;
        // 32 sectors for the root dir
        bpb.fatSz16 = 0;
        bpb.fatSz32 = 0x8000;
        bpb.numFATs = 2;
        // 0x8000 sectors per FAT so 0x10000 sectors for the FAT
        bpb.rsvdSecCnt = 32;
        bpb.secPerClus = 1;
        bpb.totSec16   = 0;

        int numReservedSectors = bpb.fatSz32 * bpb.numFATs + bpb.rsvdSecCnt;

        AND_GIVEN("Number of data clusters 65524")
        {
            bpb.totSec32 = 65524 + numReservedSectors;
            THEN("FAT16 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT16); }
        }
        AND_GIVEN("Number of data clusters 65525")
        {
            bpb.totSec32 = 65525 + numReservedSectors;
            THEN("FAT32 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT32); }
        }
        AND_GIVEN("Number of data clusters 0x1000000")
        {
            bpb.totSec32 = 0x1000000 + numReservedSectors;
            THEN("FAT32 should be detected") { REQUIRE(getFatType(bpb) == fatpp::FAT32); }
        }
    }
}