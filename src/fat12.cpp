/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file provides the implementation for FAT12
 ****************************************************************************/

#include <fatpp/detail/fat.hpp>

using namespace fatpp::detail;
using namespace fatpp;

Result<ClusterAddress> Fat12::getNextCluster(ClusterAddress addr) const
{
    // Multiply by 1.5 without using floating point.
    auto fatOffset = clusterToFatOffset(addr);
    auto fatAddr   = SectorCount{fatOffset >> _state.sectorSize};

    auto offsetInSector = fatOffset & _state.sectorMask;

    // Read the sector.
    auto sectorResult = getFatSector(fatAddr);
    if(!sectorResult)
    {
        // Failed to read the sector
        return sectorResult.status();
    }

    // Read the first half of the next cluster address
    auto nextLsb = sectorResult.result()->data()[offsetInSector];

    // Check for the edge case where the entry spans 2 sectors
    // This is the case when the offset in the sector is odd and the last byte in the sector.
    // Luckily this value has been precalculated as the sectorMask.
    std::byte nextMsb;
    if(offsetInSector == _state.sectorMask)
    {
        sectorResult = getFatSector(fatAddr + SectorCount{1});
        if(!sectorResult)
        {
            // Failed to read the sector
            return sectorResult.status();
        }

        // Because the cluster was spanning the sector boundaries the msb is the first byte of the
        // sector.
        nextMsb = sectorResult.result()->data()[0];
    }
    else
    {
        // The MSB is the next byte.
        nextMsb = sectorResult.result()->data()[offsetInSector + 1];
    }

    // Combine the LSB and MSB
    auto next = static_cast<ClusterAddress::base_type>(nextLsb) |
                (static_cast<ClusterAddress::base_type>(nextMsb) << 8);

    bool isOdd = (addr.get() & 1) != 0;
    return ClusterAddress{isOdd ? (next >> 4) : (next & 0xFFF)};
}

Result<ClusterAddress> Fat12::getFreeCluster() const
{
    // Get the number of sectors for the valid FAT. This may exclude the last sector if it is partly
    // used. This will be handled seperately.
    auto fatByteSize   = clusterToFatOffset(_state.fsSize);
    auto fatSectorSize = (fatByteSize + _state.sectorMask) >> _state.sectorSize;

    // Get the starting point to check.
    auto firstCheckFatOffset    = clusterToFatOffset(_state.lastFreeCluster);
    auto firstSectorToCheck     = firstCheckFatOffset >> _state.sectorSize;
    auto firstByteOffsetToCheck = firstCheckFatOffset & _state.sectorMask;

    ClusterAddress::base_type clusterVal;
    ClusterAddress            clusterIdx = _state.lastFreeCluster;
    bool                      isSplit    = false;

    auto getNextSector    = [&](unsigned addr) { return addr == fatSectorSize - 1 ? 0 : addr + 1; };
    auto isLastClusterIdx = [&](ClusterAddress addr)
    { return addr.get() == _state.fsSize.get() - 1; };
    auto isOddCluster  = [](ClusterAddress addr) { return (addr.get() & 1) != 0; };
    auto isFreeCluster = [](ClusterAddress::base_type val) { return val == 0; };

    // If the starting point is an edge case which spans two sectors, load the first half
    if(firstByteOffsetToCheck == _state.sectorMask)
    {
        auto sectorResult = getFatSector(SectorCount{firstSectorToCheck});
        if(!sectorResult)
        {
            // Failed to read the sector
            return sectorResult.status();
        }
        clusterVal         = static_cast<uint8_t>(sectorResult.result()->data()[_state.sectorMask]);
        isSplit            = true;
        firstSectorToCheck = getNextSector(firstSectorToCheck);
        firstByteOffsetToCheck = 1;
        // We cannot be at the first sector, because that would mean that the last partial fat entry
        // was split to the first.
        assert(firstSectorToCheck != 0);
    }

    // Iterate over all the sectors, and check the first sector twice when the fs is full
    for(SectorCount::base_type sectorAttempt = 0; sectorAttempt <= fatSectorSize; ++sectorAttempt)
    {
        auto sectorIdx = sectorAttempt + firstSectorToCheck;
        if(sectorIdx >= fatSectorSize)
            sectorIdx -= fatSectorSize;

        // Read the next sector.
        auto sectorResult = getFatSector(SectorCount{sectorIdx});
        if(!sectorResult)
        {
            // Failed to read the sector
            return sectorResult.status();
        }
        auto &sector = *sectorResult.result();

        // If we are processing a split cluster, handle it now
        if(isSplit)
        {
            clusterVal |= static_cast<uint8_t>(sector[0]) << 8;
            clusterVal = isOddCluster(clusterIdx) ? (clusterVal >> 4) : (clusterVal & 0xFFF);
            if(isFreeCluster(clusterVal))
            {
                return clusterIdx;
            }
            clusterIdx =
                isLastClusterIdx(clusterIdx) ? ClusterAddress{2} : (clusterIdx + ClusterCount{1});
        }

        // First 2 entries are not valid options.
        if(sectorIdx == 0 && (clusterIdx == ClusterAddress{0} || clusterIdx == ClusterAddress{1}))
            [[unlikely]]
        {
            clusterIdx             = ClusterAddress{2};
            firstByteOffsetToCheck = 3;
            isSplit                = false;
        }

        for(unsigned byteIdx = firstByteOffsetToCheck; byteIdx <= _state.sectorMask;
            byteIdx += isOddCluster(clusterIdx) ? 2 : 1)
        {
            if(byteIdx == _state.sectorMask) [[unlikely]]
            {
                clusterVal = static_cast<uint8_t>(sector[byteIdx]);
                isSplit    = true;
                break;
            }

            // We always read the LSB first so, while writing the MSB we or it with the stored
            // value.
            clusterVal = (static_cast<uint8_t>(sector[byteIdx + 1]) << 8) |
                         static_cast<uint8_t>(sector[byteIdx]);
            clusterVal = isOddCluster(clusterIdx) ? (clusterVal >> 4) : (clusterVal & 0xFFF);
            if(isFreeCluster(clusterVal))
            {
                return clusterIdx;
            }

            // Prepare for the next cluster
            isSplit = false;
            if(isLastClusterIdx(clusterIdx)) [[unlikely]]
            {
                // This was the last cluster. so stop with this sector.
                clusterIdx = ClusterAddress{0};
                break;
            }
            else
            {
                clusterIdx += ClusterCount{1};
            }
        }

        // Next sector start at the first byte.
        firstByteOffsetToCheck = 0;
    }

    return ErrorCode::NoSpace;
}

SimpleResult Fat12::setCluster(ClusterAddress addr, ClusterAddress val)
{
    // Get the byte address from the cluster address
    auto fatOffset      = clusterToFatOffset(addr);
    auto fatAddr        = SectorCount{fatOffset >> _state.sectorSize};
    auto offsetInSector = fatOffset & _state.sectorMask;

    // Get some parameters
    auto isOdd  = (addr.get() & 1) != 0;
    auto rawVal = val.get();

    // Read the sector
    auto sectorResult = getFatSector(fatAddr);
    if(!sectorResult)
    {
        // Failed to read the sector
        return sectorResult.status();
    }
    auto lsbSector = std::move(*sectorResult);

    // Set the LSB
    auto    curLsbVal = static_cast<uint8_t>(lsbSector->data()[offsetInSector]);
    uint8_t newLsbVal = isOdd ? (((rawVal << 4) | (curLsbVal & 0xF)) & 0xFF) : (rawVal & 0xFF);

    if(curLsbVal != newLsbVal)
    {
        // Modify the cluster if value is different
        lsbSector->data()[offsetInSector] = std::byte{newLsbVal};
        lsbSector->markDirty();
    }

    std::shared_ptr<Sector> msbSector;

    // Set the MSB
    if(offsetInSector == _state.sectorMask)
    {
        // The edge case where the cluster spans 2 sectors
        offsetInSector = 0;

        // Release the LSB sector
        lsbSector.reset();

        // Read the sector
        auto sectorResult = getFatSector(fatAddr + SectorCount{1});
        if(!sectorResult)
        {
            // Failed to read the sector
            return sectorResult.status();
        }

        msbSector = std::move(*sectorResult);
    }
    else
    {
        // The normal case. can reuse the current sector
        offsetInSector++;

        // The MSB sector is the LSB sector
        msbSector = std::move(lsbSector);
    }

    // Set the MSB
    auto    curMsbVal = static_cast<uint8_t>(msbSector->data()[offsetInSector]);
    uint8_t newMsbVal =
        isOdd ? ((rawVal >> 4) & 0xFF) : ((((rawVal >> 8) & 0xF) | (curMsbVal & 0xF0)) & 0xFF);

    if(curMsbVal != newMsbVal)
    {
        // Modify the cluster if value is different
        msbSector->data()[offsetInSector] = std::byte{newMsbVal};
        msbSector->markDirty();
    }

    // Return the result
    return ErrorCode::Ok;
}