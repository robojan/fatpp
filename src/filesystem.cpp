/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/****************************************************************************
 * This function contains the implementation of the filesystem class and the
 * supporting functions of those.
 ****************************************************************************/

#include <fatpp/filesystem.hpp>
#include <util/util.hpp>

using namespace fatpp;

namespace fatpp::detail
{
bool isValidBpb(const structure::BiosParameterBlock &bpb, uint32_t diskSize)
{
    if(bpb.bytsPerSec != 512 && bpb.bytsPerSec != 1024 && bpb.bytsPerSec != 2048 &&
       bpb.bytsPerSec != 4096)
        return false;


    if(bpb.secPerClus == 0 || !rj::isPowerOf2(bpb.secPerClus))
        return false;


    if(bpb.bytsPerSec * bpb.secPerClus > 32 * 1024)
        return false;


    if(bpb.rsvdSecCnt == 0)
        return false;

    if(bpb.numFATs == 0)
        return false;

    auto size = getTotalSectors(bpb);
    if(diskSize > 0 && size > diskSize)
        return false;

    switch(getFatType(bpb))
    {
    case FAT12:
    case FAT16:
    {
        auto &bpb12 = static_cast<const structure::BiosParameterBlockFat12 &>(bpb);
        if(bpb12.bootSig != structure::BiosParameterBlockFat12::c_bootSigValue)
            return false;
        if(bpb12.signature != structure::BiosParameterBlockFat12::c_sigValue)
            return false;
        break;
    }
    case FAT32:
    {
        auto &bpb32 = static_cast<const structure::BiosParameterBlockFat32 &>(bpb);
        if(bpb32.fatSz16 != 0)
            return false;
        if(bpb32.fsVer > 0)
            return false;
        if(bpb32.bootSig != structure::BiosParameterBlockFat12::c_bootSigValue)
            return false;
        if(bpb32.signature != structure::BiosParameterBlockFat12::c_sigValue)
            return false;
        break;
    }
    default:
        return false;
    }

    return true;
}

FileSystemType getFatType(const structure::BiosParameterBlock &bpb)
{
    auto rootDirSectors = ((bpb.rootEntCnt * 32) + (bpb.bytsPerSec - 1)) / bpb.bytsPerSec;

    // If the fatsz16 is 0, then it must be fat32. So the getFatSize function will only access the
    // fatsz32 field when it assumes it is fat32.
    auto fatSize = getFatSize(static_cast<const structure::BiosParameterBlockFat32 &>(bpb));

    auto totalSectors = getTotalSectors(bpb);

    auto numDataSectors =
        totalSectors - (bpb.rsvdSecCnt + (bpb.numFATs * fatSize) + rootDirSectors);

    auto countOfClusters = numDataSectors / bpb.secPerClus;
    if(countOfClusters < 4085)
    {
        return FAT12;
    }
    else if(countOfClusters < 65525)
    {
        return FAT16;
    }
    else
    {
        return FAT32;
    }
}

} // namespace fatpp::detail
