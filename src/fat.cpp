/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * Includes the implementation for IFat
 ****************************************************************************/

#include <fatpp/detail/fat.hpp>

namespace fatpp::detail
{

Result<std::shared_ptr<Sector>> IFat::getFatSector(SectorCount offset) const
{
    // Get the first FAT
    auto result = _state.disk.read(_state.fatLocations[0] + offset);

    // Add the alternative FAT addresses to the sector
    if(result)
    {
        for(auto it = _state.fatLocations.begin() + 1; it != _state.fatLocations.end(); ++it)
        {
            result.result()->addAddress(*it + offset);
        }
    }

    return result;
}

FatIterator IFat::getChain(ClusterAddress addr) const
{
    assert(addr.get() > 1);
    return FatIterator(*this, addr);
}

} // namespace fatpp::detail