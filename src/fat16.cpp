/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file provides the implementation for FAT16
 ****************************************************************************/

#include <fatpp/detail/fat.hpp>
#include <util/endian.hpp>

using namespace fatpp::detail;
using namespace fatpp;

Result<ClusterAddress> Fat16::getNextCluster(ClusterAddress addr) const
{
    // Get the byte address from the cluster address
    auto fatOffset      = addr.get() * 2;
    auto fatAddr        = SectorCount{fatOffset >> _state.sectorSize};
    auto offsetInSector = fatOffset & _state.sectorMask;

    // Read the sector
    auto sectorResult = getFatSector(fatAddr);
    if(!sectorResult)
    {
        // Failed to read the sector
        return sectorResult.status();
    }

    // Read the cluster
    auto srcAddr =
        reinterpret_cast<const uint16_t *>(&sectorResult.result()->data()[offsetInSector]);
    auto rawValue = rj::fromLittle(*srcAddr);

    // Return the result
    return ClusterAddress{rawValue};
}

Result<ClusterAddress> Fat16::getFreeCluster() const
{
    // Get the number of sectors for the valid FAT. This may exclude the last sector if it is partly
    // used. This will be handled seperately.
    auto fatByteSize   = _state.fsSize.get() * 2;
    auto fatSectorSize = (fatByteSize + _state.sectorMask) >> _state.sectorSize;

    // Get the starting point to check.
    auto firstCheckFatOffset    = _state.lastFreeCluster.get() * 2;
    auto firstSectorToCheck     = firstCheckFatOffset >> _state.sectorSize;
    auto firstByteOffsetToCheck = firstCheckFatOffset & _state.sectorMask;

    // State variables
    ClusterAddress clusterIdx = _state.lastFreeCluster;

    // Helper functions
    auto getNextSector    = [&](unsigned addr) { return addr == fatSectorSize - 1 ? 0 : addr + 1; };
    auto isLastClusterIdx = [&](ClusterAddress addr)
    { return addr.get() == _state.fsSize.get() - 1; };
    auto isFreeCluster = [](ClusterAddress::base_type val) { return val == 0; };

    // Iterate over all the sectors, and check the first sector twice when the FS is full
    for(SectorCount::base_type sectorAttempt = 0; sectorAttempt <= fatSectorSize; ++sectorAttempt)
    {
        auto sectorIdx = sectorAttempt + firstSectorToCheck;
        if(sectorIdx >= fatSectorSize)
            sectorIdx -= fatSectorSize;

        // Read the next sector.
        auto sectorResult = getFatSector(SectorCount{sectorIdx});
        if(!sectorResult)
        {
            // Failed to read the sector
            return sectorResult.status();
        }
        auto &sector = *sectorResult.result();

        // First 2 entries are not valid options.
        if(sectorIdx == 0 && (clusterIdx == ClusterAddress{0} || clusterIdx == ClusterAddress{1}))
            [[unlikely]]
        {
            clusterIdx             = ClusterAddress{2};
            firstByteOffsetToCheck = 4;
        }

        // Iterate all the clusters in the sector
        for(unsigned byteIdx = firstByteOffsetToCheck; byteIdx <= _state.sectorMask; byteIdx += 2)
        {
            // Read the cluster
            // Because we are compare to 0, we don't care about endianness.
            auto clusterVal = *reinterpret_cast<const uint16_t *>(&sector[byteIdx]);
            if(isFreeCluster(clusterVal))
            {
                return clusterIdx;
            }

            // Prepare for the next cluster
            if(isLastClusterIdx(clusterIdx)) [[unlikely]]
            {
                // This was the last cluster. so stop with this sector.
                clusterIdx = ClusterAddress{0};
                break;
            }
            else
            {
                clusterIdx += ClusterCount{1};
            }
        }

        // Next sector start at the first byte.
        firstByteOffsetToCheck = 0;
    }

    // We failed to find a free cluster.
    return ErrorCode::NoSpace;
}

SimpleResult Fat16::setCluster(ClusterAddress addr, ClusterAddress val)
{
    // Get the byte address from the cluster address
    auto fatOffset      = addr.get() * 2;
    auto fatAddr        = SectorCount{fatOffset >> _state.sectorSize};
    auto offsetInSector = fatOffset & _state.sectorMask;

    // Read the sector
    auto sectorResult = getFatSector(fatAddr);
    if(!sectorResult)
    {
        // Failed to read the sector
        return sectorResult.status();
    }

    // Read the cluster
    auto srcAddr  = reinterpret_cast<uint32_t *>(&sectorResult.result()->data()[offsetInSector]);
    auto rawValue = rj::fromLittle(*srcAddr);

    if(rawValue != val.get())
    {
        // Modify the cluster if value is different
        rawValue = val.get();
        *srcAddr = rj::toLittle(rawValue);
        sectorResult.result()->markDirty();
    }

    // Return the result
    return ErrorCode::Ok;
}
