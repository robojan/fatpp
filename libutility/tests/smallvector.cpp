/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * Compiler specific tests
 *
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <util/smallvector.hpp>

using namespace rj;

SCENARIO("A SmallVector must be constructable", "[SmallVector]")
{
    GIVEN("A default constructed SmallVector<int,5>")
    {
        SmallVector<int, 5> v;
        THEN("Capacity must be valid")
        {
            REQUIRE(v.capacity() == 5);
            REQUIRE(v.max_size() == 5);
        }
        THEN("Size must be 0") { REQUIRE(v.size() == 0); }
        THEN("Iterators must be valid")
        {
            REQUIRE(v.begin() == v.end());
            REQUIRE(v.begin() == v.data());
        }
    }
    GIVEN("A SmallVector<int,5> v(3, 200)")
    {
        SmallVector<int, 5> v(3, 200);
        THEN("Size must be 3") { REQUIRE(v.size() == 3); }
        THEN("Iterators must be valid")
        {
            REQUIRE(v.begin() == v.data());
            REQUIRE(v.data() + 3 == v.end());
        }
        THEN("The value must be 200")
        {
            REQUIRE(std::all_of(v.begin(), v.end(), [](int x) { return x == 200; }));
        }
        THEN("The copy constructor should be able to copy the vector to a vector of capacity 5")
        {
            SmallVector<int, 5> v2(v);
            THEN("Size must be 3") { REQUIRE(v2.size() == 3); }
            THEN("Iterators must be valid")
            {
                REQUIRE(v2.begin() == v2.data());
                REQUIRE(v2.data() + 3 == v2.end());
            }
            THEN("The value must be 200")
            {
                REQUIRE(std::all_of(v2.begin(), v2.end(), [](int x) { return x == 200; }));
            }
            THEN("The old vector must be the same size") { REQUIRE(v.size() == 3); }
        }
        THEN("The copy constructor should be able to copy the vector to a vector of capacity 3")
        {
            SmallVector<int, 3> v2(v);
            THEN("Size must be 3") { REQUIRE(v2.size() == 3); }
            THEN("Iterators must be valid")
            {
                REQUIRE(v2.begin() == v2.data());
                REQUIRE(v2.data() + 3 == v2.end());
            }
            THEN("The value must be 200")
            {
                REQUIRE(std::all_of(v2.begin(), v2.end(), [](int x) { return x == 200; }));
            }
            THEN("The old vector must be the same size") { REQUIRE(v.size() == 3); }
        }
        THEN("The move constructor should be able to move the vector to a vector of capacity 5")
        {
            SmallVector<int, 5> v2(std::move(v));
            THEN("Size must be 3") { REQUIRE(v2.size() == 3); }
            THEN("Iterators must be valid")
            {
                REQUIRE(v2.begin() == v2.data());
                REQUIRE(v2.data() + 3 == v2.end());
            }
            THEN("The value must be 200")
            {
                REQUIRE(std::all_of(v2.begin(), v2.end(), [](int x) { return x == 200; }));
            }
            THEN("The old vector must be empty") { REQUIRE(v.size() == 0); }
        }
        THEN("The move constructor should be able to move the vector to a vector of capacity 3")
        {
            SmallVector<int, 3> v2(std::move(v));
            THEN("Size must be 3") { REQUIRE(v2.size() == 3); }
            THEN("Iterators must be valid")
            {
                REQUIRE(v2.begin() == v2.data());
                REQUIRE(v2.data() + 3 == v2.end());
            }
            THEN("The value must be 200")
            {
                REQUIRE(std::all_of(v2.begin(), v2.end(), [](int x) { return x == 200; }));
            }
            THEN("The old vector must be empty") { REQUIRE(v.size() == 0); }
        }
        THEN("The range constructor should be able to copy the vector to a vector of capacity 3")
        {
            SmallVector<int, 3> v2(v.begin(), v.end());
            THEN("Size must be 3") { REQUIRE(v2.size() == 3); }
            THEN("Iterators must be valid")
            {
                REQUIRE(v2.begin() == v2.data());
                REQUIRE(v2.data() + 3 == v2.end());
            }
            THEN("The value must be 200")
            {
                REQUIRE(std::all_of(v2.begin(), v2.end(), [](int x) { return x == 200; }));
            }
            THEN("The old vector must be the same size") { REQUIRE(v.size() == 3); }
        }
    }
    GIVEN("A SmallVector<int,5> v(3)")
    {
        SmallVector<int, 5> v(3);
        THEN("Size must be 3") { REQUIRE(v.size() == 3); }
        THEN("Iterators must be valid")
        {
            REQUIRE(v.begin() == v.data());
            REQUIRE(v.data() + 3 == v.end());
        }
        THEN("The value must be 0")
        {
            REQUIRE(std::all_of(v.begin(), v.end(), [](int x) { return x == 0; }));
        }
    }
    GIVEN("A SmallVector<int,5> v{4}")
    {
        SmallVector<int, 5> v{4};
        THEN("Size must be 1") { REQUIRE(v.size() == 1); }
        THEN("Iterators must be valid")
        {
            REQUIRE(v.begin() == v.data());
            REQUIRE(v.data() + 1 == v.end());
        }
        THEN("The value must be 4") { REQUIRE(v[0] == 4); }
    }
    GIVEN("A SmallVector<int,5> v{4,5,6,7}")
    {
        SmallVector<int, 5> v{4, 5, 6, 7};
        THEN("Size must be 4") { REQUIRE(v.size() == 4); }
        THEN("Iterators must be valid")
        {
            REQUIRE(v.begin() == v.data());
            REQUIRE(v.data() + 4 == v.end());
        }
        THEN("The v[0] must be 4") { REQUIRE(v[0] == 4); }
        THEN("The v[1] must be 5") { REQUIRE(v[1] == 5); }
        THEN("The v[2] must be 6") { REQUIRE(v[2] == 6); }
        THEN("The v[3] must be 7") { REQUIRE(v[3] == 7); }
    }
}

SCENARIO("SmallVector assignment must work", "[SmallVector]")
{
    GIVEN("A SmallVector<int,5> v1{1,2,3} and SmallVector<int,5> v2{10,20}")
    {
        SmallVector<int, 5> v1{1, 2, 3};
        SmallVector<int, 5> v2{10, 20};
        THEN("Assigning v2 to v1, should keep v2 intact")
        {
            auto &ref = (v1 = v2);
            REQUIRE(v2.size() == 2);
            REQUIRE(v2[0] == 10);
            REQUIRE(v2[1] == 20);
            REQUIRE(std::addressof(ref) == std::addressof(v1));
        }
        THEN("Assigning v2 to v1, should modify v1")
        {
            auto &ref = (v1 = v2);
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 10);
            REQUIRE(v1[1] == 20);
            REQUIRE(std::addressof(ref) == std::addressof(v1));
        }
        THEN("moving v2 to v1, should empty v2 intact")
        {
            auto &ref = (v1 = std::move(v2));
            REQUIRE(v2.size() == 0);
            REQUIRE(std::addressof(ref) == std::addressof(v1));
        }
        THEN("moving v2 to v1, should modify v1")
        {
            auto &ref = (v1 = std::move(v2));
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 10);
            REQUIRE(v1[1] == 20);
            REQUIRE(std::addressof(ref) == std::addressof(v1));
        }
        THEN("assign(v2.begin(), v2.end()) should change the vector to contain 10,20")
        {
            v1.assign(v2.begin(), v2.end());
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 10);
            REQUIRE(v1[1] == 20);
        }
    }
    GIVEN("A Smallvector<int,5> v1{1,2,3}")
    {
        SmallVector<int, 5> v1{1, 2, 3};
        THEN("Assigning an initializer list must work")
        {
            auto &ref = (v1 = {50, 51, 52});
            REQUIRE(v1.size() == 3);
            REQUIRE(v1[0] == 50);
            REQUIRE(v1[1] == 51);
            REQUIRE(v1[2] == 52);
            REQUIRE(std::addressof(ref) == std::addressof(v1));
        }
        THEN("assign(3,2) should change the vector to size 3 with value 2")
        {
            v1.assign(3, 2);
            REQUIRE(v1.size() == 3);
            REQUIRE(v1[0] == 2);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 2);
        }
        THEN("assign({3,2}) should change the vector to contain 3,2")
        {
            v1.assign({3, 2});
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 3);
            REQUIRE(v1[1] == 2);
        }
    }
}

SCENARIO("SmallVector value retrieval should work", "[SmallVector]")
{
    GIVEN("A SmallVector<int,5> v1{1,2,3} ")
    {
        SmallVector<int, 5> v1{1, 2, 3};
        THEN("at return the elements of the container")
        {
            REQUIRE(v1.at(0) == 1);
            REQUIRE(v1.at(1) == 2);
            REQUIRE(v1.at(2) == 3);
        }
        THEN("[] return the elements of the container")
        {
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
        }
        THEN("front returns the first element") { REQUIRE(v1.front() == 1); }
        THEN("back returns the last element") { REQUIRE(v1.back() == 3); }
        THEN("data returns a pointer to the first element")
        {
            auto data = v1.data();
            REQUIRE(data[0] == 1);
            REQUIRE(data[1] == 2);
            REQUIRE(data[2] == 3);
        }
        THEN("begin returns an iterator to the first element") { REQUIRE(v1.begin() == v1.data()); }
        THEN("cbegin returns an iterator to the first element")
        {
            REQUIRE(v1.cbegin() == v1.data());
        }
        THEN("end returns an iterator past the last element")
        {
            REQUIRE(v1.end() == v1.data() + 3);
        }
        THEN("cend returns an iterator past the last element")
        {
            REQUIRE(v1.cend() == v1.data() + 3);
        }
    }
    GIVEN("A const SmallVector<int,5> v1{1,2,3} ")
    {
        const SmallVector<int, 5> v1{1, 2, 3};
        THEN("at return the elements of the container")
        {
            REQUIRE(v1.at(0) == 1);
            REQUIRE(v1.at(1) == 2);
            REQUIRE(v1.at(2) == 3);
        }
        THEN("[] return the elements of the container")
        {
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
        }
        THEN("front returns the first element") { REQUIRE(v1.front() == 1); }
        THEN("back returns the last element") { REQUIRE(v1.back() == 3); }
        THEN("data returns a pointer to the first element")
        {
            auto data = v1.data();
            REQUIRE(data[0] == 1);
            REQUIRE(data[1] == 2);
            REQUIRE(data[2] == 3);
        }
        THEN("begin returns an iterator to the first element") { REQUIRE(v1.begin() == v1.data()); }
        THEN("cbegin returns an iterator to the first element")
        {
            REQUIRE(v1.cbegin() == v1.data());
        }
        THEN("end returns an iterator past the last element")
        {
            REQUIRE(v1.end() == v1.data() + 3);
        }
        THEN("cend returns an iterator past the last element")
        {
            REQUIRE(v1.cend() == v1.data() + 3);
        }
    }
}

SCENARIO("A SmallVector should return the correct size", "[SmallVector]")
{
    GIVEN("A SmallVector<int,5> v1(3)")
    {
        SmallVector<int, 5> v1(3);
        THEN("Size should be 3") { REQUIRE(v1.size() == 3); }
    }
    GIVEN("A SmallVector<int,5> v1(0)")
    {
        SmallVector<int, 5> v1(0);
        THEN("Size should be 0") { REQUIRE(v1.size() == 0); }
    }
    GIVEN("A SmallVector<int,5> v1(5)")
    {
        SmallVector<int, 5> v1(5);
        THEN("Size should be 5") { REQUIRE(v1.size() == 5); }
    }
}

SCENARIO("A SmallVector should be able to be cleared", "[SmallVector]")
{
    GIVEN("A SmallVector<int,5> v1(3)")
    {
        SmallVector<int, 5> v1(3);
        THEN("Size should be 3") { REQUIRE(v1.size() == 3); }
        THEN("Clearing should set the size to 0")
        {
            v1.clear();
            REQUIRE(v1.size() == 0);
        }
    }
}

SCENARIO("Elements should be able to be inserted in a SmallVector", "[SmallVector]")
{
    GIVEN("A SmallVector<int,10> v1({1,2,3})")
    {
        SmallVector<int, 10> v1({1, 2, 3});
        THEN("Size should be 3") { REQUIRE(v1.size() == 3); }
        THEN("Inserting an element at the beginning should work")
        {
            auto it = v1.insert(v1.begin(), 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 4);
            REQUIRE(v1[1] == 1);
            REQUIRE(v1[2] == 2);
            REQUIRE(v1[3] == 3);
            REQUIRE(it == v1.data() + 0);
        }
        THEN("Inserting an element at pos 1 should work")
        {
            auto it = v1.insert(v1.begin() + 1, 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 4);
            REQUIRE(v1[2] == 2);
            REQUIRE(v1[3] == 3);
            REQUIRE(it == v1.data() + 1);
        }
        THEN("Inserting an element at the end should work")
        {
            auto it = v1.insert(v1.end(), 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
            REQUIRE(it == v1.data() + 3);
        }
        THEN("Pushing an element at the back should work")
        {
            v1.push_back(4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
        }
        THEN("Inserting 3 elements at the beginning should work")
        {
            auto it = v1.insert(v1.begin(), 3, 4);
            REQUIRE(v1.size() == 6);
            REQUIRE(v1[0] == 4);
            REQUIRE(v1[1] == 4);
            REQUIRE(v1[2] == 4);
            REQUIRE(v1[3] == 1);
            REQUIRE(v1[4] == 2);
            REQUIRE(v1[5] == 3);
            REQUIRE(it == v1.data() + 0);
        }
        THEN("Inserting 3 elements at pos 1 should work")
        {
            auto it = v1.insert(v1.begin() + 1, 3, 4);
            REQUIRE(v1.size() == 6);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 4);
            REQUIRE(v1[2] == 4);
            REQUIRE(v1[3] == 4);
            REQUIRE(v1[4] == 2);
            REQUIRE(v1[5] == 3);
            REQUIRE(it == v1.data() + 1);
        }
        THEN("Inserting 3 elements at the end should work")
        {
            auto it = v1.insert(v1.end(), 3, 4);
            REQUIRE(v1.size() == 6);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
            REQUIRE(v1[4] == 4);
            REQUIRE(v1[5] == 4);
            REQUIRE(it == v1.data() + 3);
        }
        GIVEN("an initializer list")
        {
            auto val = {4, 5, 6};
            THEN("Inserting 3 elements at the beginning should work")
            {
                auto it = v1.insert(v1.begin(), val.begin(), val.end());
                REQUIRE(v1.size() == 6);
                REQUIRE(v1[0] == 4);
                REQUIRE(v1[1] == 5);
                REQUIRE(v1[2] == 6);
                REQUIRE(v1[3] == 1);
                REQUIRE(v1[4] == 2);
                REQUIRE(v1[5] == 3);
                REQUIRE(it == v1.data() + 0);
            }
            THEN("Inserting 3 elements at pos 1 should work")
            {
                auto it = v1.insert(v1.begin() + 1, val.begin(), val.end());
                REQUIRE(v1.size() == 6);
                REQUIRE(v1[0] == 1);
                REQUIRE(v1[1] == 4);
                REQUIRE(v1[2] == 5);
                REQUIRE(v1[3] == 6);
                REQUIRE(v1[4] == 2);
                REQUIRE(v1[5] == 3);
                REQUIRE(it == v1.data() + 1);
            }
            THEN("Inserting 3 elements at the end should work")
            {
                auto it = v1.insert(v1.end(), val.begin(), val.end());
                REQUIRE(v1.size() == 6);
                REQUIRE(v1[0] == 1);
                REQUIRE(v1[1] == 2);
                REQUIRE(v1[2] == 3);
                REQUIRE(v1[3] == 4);
                REQUIRE(v1[4] == 5);
                REQUIRE(v1[5] == 6);
                REQUIRE(it == v1.data() + 3);
            }
            THEN("Inserting the initializer list at the beginning should work")
            {
                auto it = v1.insert(v1.begin(), val);
                REQUIRE(v1.size() == 6);
                REQUIRE(v1[0] == 4);
                REQUIRE(v1[1] == 5);
                REQUIRE(v1[2] == 6);
                REQUIRE(v1[3] == 1);
                REQUIRE(v1[4] == 2);
                REQUIRE(v1[5] == 3);
                REQUIRE(it == v1.data() + 0);
            }
            THEN("Inserting 3 elements at pos 1 should work")
            {
                auto it = v1.insert(v1.begin() + 1, val);
                REQUIRE(v1.size() == 6);
                REQUIRE(v1[0] == 1);
                REQUIRE(v1[1] == 4);
                REQUIRE(v1[2] == 5);
                REQUIRE(v1[3] == 6);
                REQUIRE(v1[4] == 2);
                REQUIRE(v1[5] == 3);
                REQUIRE(it == v1.data() + 1);
            }
            THEN("Inserting 3 elements at the end should work")
            {
                auto it = v1.insert(v1.end(), val);
                REQUIRE(v1.size() == 6);
                REQUIRE(v1[0] == 1);
                REQUIRE(v1[1] == 2);
                REQUIRE(v1[2] == 3);
                REQUIRE(v1[3] == 4);
                REQUIRE(v1[4] == 5);
                REQUIRE(v1[5] == 6);
                REQUIRE(it == v1.data() + 3);
            }
        }
    }
    GIVEN("A SmallVector<std::pair<int,int>, 10> v1")
    {
        SmallVector<std::pair<int, int>, 10> v1{{1, 1}, {2, 2}, {3, 3}};
        THEN("Size should be 3") { REQUIRE(v1.size() == 3); }
        THEN("Emplacing at the beginning should work")
        {
            auto it = v1.emplace(v1.begin(), 4, 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == std::pair{4, 4});
            REQUIRE(v1[1] == std::pair{1, 1});
            REQUIRE(v1[2] == std::pair{2, 2});
            REQUIRE(v1[3] == std::pair{3, 3});
            REQUIRE(it == v1.data() + 0);
        }
        THEN("Emplacing at pos 1 should work")
        {
            auto it = v1.emplace(v1.begin() + 1, 4, 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == std::pair{1, 1});
            REQUIRE(v1[1] == std::pair{4, 4});
            REQUIRE(v1[2] == std::pair{2, 2});
            REQUIRE(v1[3] == std::pair{3, 3});
            REQUIRE(it == v1.data() + 1);
        }
        THEN("Emplacing at end should work")
        {
            auto it = v1.emplace(v1.end(), 4, 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == std::pair{1, 1});
            REQUIRE(v1[1] == std::pair{2, 2});
            REQUIRE(v1[2] == std::pair{3, 3});
            REQUIRE(v1[3] == std::pair{4, 4});
            REQUIRE(it == v1.data() + 3);
        }
        THEN("emplace_back")
        {
            v1.emplace_back(4, 4);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == std::pair{1, 1});
            REQUIRE(v1[1] == std::pair{2, 2});
            REQUIRE(v1[2] == std::pair{3, 3});
            REQUIRE(v1[3] == std::pair{4, 4});
        }
    }
}

SCENARIO("Elements should be able to be removed in a SmallVector", "[SmallVector]")
{
    GIVEN("A SmallVector<int,10> v1({1,2,3,4,5})")
    {
        SmallVector<int, 10> v1({1, 2, 3, 4, 5});
        THEN("Size should be 5") { REQUIRE(v1.size() == 5); }
        THEN("Erasing the first element should work")
        {
            auto it = v1.erase(v1.begin());
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 2);
            REQUIRE(v1[1] == 3);
            REQUIRE(v1[2] == 4);
            REQUIRE(v1[3] == 5);
            REQUIRE(it == v1.data() + 0);
        }
        THEN("Erasing the element at pos 1 should work")
        {
            auto it = v1.erase(v1.begin() + 1);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 3);
            REQUIRE(v1[2] == 4);
            REQUIRE(v1[3] == 5);
            REQUIRE(it == v1.data() + 1);
        }
        THEN("Erasing the last element should work")
        {
            auto it = v1.erase(v1.end() - 1);
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
            REQUIRE(it == v1.end());
        }
        THEN("Popping an element at the back should work")
        {
            v1.pop_back();
            REQUIRE(v1.size() == 4);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
        }
        THEN("Erasing 3 elements at the beginning should work")
        {
            auto it = v1.erase(v1.begin(), v1.begin() + 3);
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 4);
            REQUIRE(v1[1] == 5);
            REQUIRE(it == v1.data() + 0);
        }
        THEN("Erasing 3 elements at pos 1 should work")
        {
            auto it = v1.erase(v1.begin() + 1, v1.begin() + 4);
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 5);
            REQUIRE(it == v1.data() + 1);
        }
        THEN("Erasing 3 elements at the end should work")
        {
            auto it = v1.erase(v1.begin() + 2, v1.end());
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(it == v1.end());
        }
    }
}

SCENARIO("A SmallVector must be able to be resized", "[SmallVector]")
{
    GIVEN("A SmallVector<int,10> v1({1,2,3,4,5})")
    {
        SmallVector<int, 10> v1({1, 2, 3, 4, 5});
        THEN("Size should be 5") { REQUIRE(v1.size() == 5); }
        THEN("Resizing to 3 should remove the last 2 elements")
        {
            v1.resize(3);
            REQUIRE(v1.size() == 3);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
        }
        THEN("Resizing to 3 with value 10 should remove the last 2 elements")
        {
            v1.resize(3, 10);
            REQUIRE(v1.size() == 3);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
        }
        THEN("Resizing to 7 should add 2 elements to the back")
        {
            v1.resize(7);
            REQUIRE(v1.size() == 7);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
            REQUIRE(v1[4] == 5);
            REQUIRE(v1[5] == 0);
            REQUIRE(v1[6] == 0);
        }
        THEN("Resizing to 7 with value 10 should add 2 elements to the back")
        {
            v1.resize(7, 10);
            REQUIRE(v1.size() == 7);
            REQUIRE(v1[0] == 1);
            REQUIRE(v1[1] == 2);
            REQUIRE(v1[2] == 3);
            REQUIRE(v1[3] == 4);
            REQUIRE(v1[4] == 5);
            REQUIRE(v1[5] == 10);
            REQUIRE(v1[6] == 10);
        }
    }
}

SCENARIO("A SmallVector must be able to be swapped", "[SmallVector]")
{
    GIVEN("A SmallVector<int,5> v1({1,2,3}) and a SmallVector<int,5> v2{10,20}")
    {
        SmallVector<int, 5> v1({1, 2, 3});
        SmallVector<int, 5> v2({10, 20});
        THEN("Swapping v1 and v2 should swap the contents")
        {
            v1.swap(v2);
            REQUIRE(v2.size() == 3);
            REQUIRE(v2[0] == 1);
            REQUIRE(v2[1] == 2);
            REQUIRE(v2[2] == 3);
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 10);
            REQUIRE(v1[1] == 20);
        }
        THEN("Swapping v2 and v1 should swap the contents")
        {
            v2.swap(v1);
            REQUIRE(v2.size() == 3);
            REQUIRE(v2[0] == 1);
            REQUIRE(v2[1] == 2);
            REQUIRE(v2[2] == 3);
            REQUIRE(v1.size() == 2);
            REQUIRE(v1[0] == 10);
            REQUIRE(v1[1] == 20);
        }
    }
}

SCENARIO("SmallVectors should be able to be compared", "[SmallVector]")
{
    GIVEN("A SmallVector<int,5> v1({1,2,3}) and a SmallVector<int,5> v2{1,2,3}")
    {
        SmallVector<int, 5> v1({1, 2, 3});
        SmallVector<int, 5> v2({1, 2, 3});
        THEN("They should be equal")
        {
            REQUIRE(v1 == v2);
            REQUIRE(v2 == v1);
            REQUIRE(v2 <= v1);
            REQUIRE(v2 >= v1);
            REQUIRE(v1 <= v2);
            REQUIRE(v1 >= v2);
        }
    }
    GIVEN("A SmallVector<int,8> v1({1,2,3}) and a SmallVector<int,5> v2{1,2,3}")
    {
        SmallVector<int, 8> v1({1, 2, 3});
        SmallVector<int, 5> v2({1, 2, 3});
        THEN("They should be equal")
        {
            REQUIRE(v1 == v2);
            REQUIRE(v2 == v1);
            REQUIRE(v2 <= v1);
            REQUIRE(v2 >= v1);
            REQUIRE(v1 <= v2);
            REQUIRE(v1 >= v2);
        }
    }
    GIVEN("A SmallVector<int,5> v1({1,2,3,4}) and a SmallVector<int,5> v2{1,2,3}")
    {
        SmallVector<int, 5> v1({1, 2, 3, 4});
        SmallVector<int, 5> v2({1, 2, 3});
        THEN("They should be unequal")
        {
            REQUIRE(v1 != v2);
            REQUIRE(v2 != v1);
            REQUIRE(v1 > v2);
            REQUIRE(v2 < v1);
        }
    }
    GIVEN("A SmallVector<int,5> v1({1,1,3}) and a SmallVector<int,5> v2{1,2,3}")
    {
        SmallVector<int, 5> v1({1, 1, 3});
        SmallVector<int, 5> v2({1, 2, 3});
        THEN("They should be unequal")
        {
            REQUIRE(v1 != v2);
            REQUIRE(v2 != v1);
            REQUIRE(v1 < v2);
            REQUIRE(v2 > v1);
        }
    }
}
