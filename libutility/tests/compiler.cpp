/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * Compiler specific tests
 *
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <util/compiler.hpp>
#include <util/endian.hpp>

TEST_CASE("byteswap", "[compiler]")
{
    // Test if the byteswap operation is working
    using namespace rj;
    REQUIRE(byteswap((uint8_t)0x12) == 0x12);
    REQUIRE(byteswap((uint16_t)0x1234) == 0x3412);
    REQUIRE(byteswap(0x12345678U) == 0x78563412U);
    REQUIRE(byteswap(0x123456789abcdef0ULL) == 0xf0debc9a78563412ULL);
}

TEST_CASE("endian", "[compiler]")
{
    // Test if the endianness conversion is working
    using namespace rj;
    if constexpr(kIsLittleEndian)
    {
        REQUIRE(fromLittle(0x12345678) == 0x12345678);
        REQUIRE(fromBig(0x12345678) == 0x78563412);
        REQUIRE(toLittle(0x12345678) == 0x12345678);
        REQUIRE(toBig(0x12345678) == 0x78563412);

        REQUIRE(fromLittle((uint16_t)0x1234) == 0x1234);
        REQUIRE(fromBig((uint16_t)0x1234) == 0x3412);
        REQUIRE(toLittle((uint16_t)0x1234) == 0x1234);
        REQUIRE(toBig((uint16_t)0x1234) == 0x3412);
    }
    else
    {
        REQUIRE(fromLittle(0x12345678) == 0x78563412);
        REQUIRE(fromBig(0x12345678) == 0x12345678);
        REQUIRE(toLittle(0x12345678) == 0x78563412);
        REQUIRE(toBig(0x12345678) == 0x12345678);

        REQUIRE(fromLittle((uint16_t)0x1234) == 0x3412);
        REQUIRE(fromBig((uint16_t)0x1234) == 0x1234);
        REQUIRE(toLittle((uint16_t)0x1234) == 0x3412);
        REQUIRE(toBig((uint16_t)0x1234) == 0x1234);
    }
}
