/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains tests for the type classes
 ****************************************************************************/

#include <catch2/catch.hpp>
#include <fatpp/detail/types.hpp>

using namespace fatpp::detail;

using TestNumType1 = rj::NamedNumericType<int, struct TestTag1>;
using TestPtrType1 = rj::NamedPointerType<int, TestNumType1, struct TestTag2>;

TEST_CASE("NamedPointerType constructor", "[types]")
{
    TestPtrType1 a;
    REQUIRE(a.get() == 0);

    TestPtrType1 b{125};
    REQUIRE(b.get() == 125);
}

TEST_CASE("NamedPointerType +", "[types]")
{
    TestPtrType1 a{100};
    REQUIRE(a.get() == 100);

    TestPtrType1 b = a + TestNumType1{25};
    REQUIRE(b.get() == 125);
}

TEST_CASE("NamedPointerType -", "[types]")
{
    TestPtrType1 a{100};
    REQUIRE(a.get() == 100);

    TestPtrType1 b = a - TestNumType1{25};
    REQUIRE(b.get() == 75);
}

TEST_CASE("NamedPointerType +=", "[types]")
{
    TestPtrType1 a{100};
    REQUIRE(a.get() == 100);

    a += TestNumType1{25};
    REQUIRE(a.get() == 125);
}

TEST_CASE("NamedPointerType -=", "[types]")
{
    TestPtrType1 a{100};
    REQUIRE(a.get() == 100);

    a -= TestNumType1{25};
    REQUIRE(a.get() == 75);
}

TEST_CASE("NamedNumericType constructor", "[types]")
{
    TestNumType1 a;
    REQUIRE(a.get() == 0);

    TestNumType1 b{125};
    REQUIRE(b.get() == 125);
}

TEST_CASE("NamedNumericType +", "[types]")
{
    TestNumType1 a{100};
    REQUIRE(a.get() == 100);

    TestNumType1 b = a + TestNumType1{25};
    REQUIRE(b.get() == 125);
}

TEST_CASE("NamedNumericType -", "[types]")
{
    TestNumType1 a{100};
    REQUIRE(a.get() == 100);

    TestNumType1 b = a - TestNumType1{25};
    REQUIRE(b.get() == 75);
}

TEST_CASE("NamedNumericType +=", "[types]")
{
    TestNumType1 a{100};
    REQUIRE(a.get() == 100);

    a += TestNumType1{25};
    REQUIRE(a.get() == 125);
}

TEST_CASE("NamedNumericType -=", "[types]")
{
    TestNumType1 a{100};
    REQUIRE(a.get() == 100);

    a -= TestNumType1{25};
    REQUIRE(a.get() == 75);
}
