/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * This file contains functions to convert values to and from a
 * specified endianness
 *
 ****************************************************************************/
#pragma once

#include <bit>
#include <util/compiler.hpp>

namespace rj
{
static constexpr bool kIsLittleEndian = std::endian::native == std::endian::little;

template <typename T>
inline constexpr T toLittle(T x) noexcept requires std::integral<T> || std::floating_point<T>
{
    if constexpr(kIsLittleEndian)
    {
        return x;
    }
    else
    {
        return byteswap(x);
    }
}

template <typename T>
inline constexpr T fromLittle(T x) noexcept requires std::integral<T> || std::floating_point<T>
{
    if constexpr(kIsLittleEndian)
    {
        return x;
    }
    else
    {
        return byteswap(x);
    }
}

template <typename T>
inline constexpr T toBig(T x) noexcept requires std::integral<T> || std::floating_point<T>
{
    if constexpr(!kIsLittleEndian)
    {
        return x;
    }
    else
    {
        return byteswap(x);
    }
}

template <typename T>
inline constexpr T fromBig(T x) noexcept requires std::integral<T> || std::floating_point<T>
{
    if constexpr(!kIsLittleEndian)
    {
        return x;
    }
    else
    {
        return byteswap(x);
    }
}

} // namespace rj
