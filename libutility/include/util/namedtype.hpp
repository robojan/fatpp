/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains template classes for custom strongly typed named types.
 ****************************************************************************/
#pragma once

#include <utility>
#include <ostream>

namespace rj
{

/*****************************************************************************
 * NamedPointerType is a strongly typed pointer class.
 *
 * The NamedPointerType is meant to be a safe class which implements a pointer type.
 * This pointer type can be compared against itself, but no arithmatic operation may
 * performed with objects of the same type, only with types of SizeType.
 *
 * It is recommended that you make an alias of this class with your parameters. e.g.
 * ~~~~~{.cpp}
 * using SectorAddress = detail::NamedPointerType<detail::addr_t, SectorCount, struct
 * SectorAddressTag>;
 * ~~~~~
 *
 * @param T The storage type.
 * @param SizeType The type which may be added or substracted from this type.
 *      It should implement a get function which can be added or subtracted from T.
 * @param Tag Tag to make the type unique and prevent conversions.
 ****************************************************************************/
template <typename T, typename SizeType, typename Tag>
class NamedPointerType
{
public:
    using base_type = T;

    /*****************************************************************************
     * Create a new instance of the NamedPointerType with the specified value
     *
     * @param val The initial value
     ****************************************************************************/
    constexpr explicit NamedPointerType(const T &val = {}) : _val(val) {}

    /*****************************************************************************
     * Return the current value in the underlying type.
     *
     * @return The current value in the underlying type.
     ****************************************************************************/
    [[nodiscard]] constexpr const T &get() const { return _val; }

    constexpr NamedPointerType &operator++()
    {
        ++_val;
        return *this;
    }

    constexpr NamedPointerType operator++(int)
    {
        auto self = *this;
        ++_val;
        return self;
    }

    constexpr NamedPointerType &operator--()
    {
        --_val;
        return *this;
    }

    constexpr NamedPointerType operator--(int)
    {
        auto self = *this;
        --_val;
        return self;
    }


private:
    T _val; //< The stored value.

    [[nodiscard]] friend constexpr auto operator<=>(const NamedPointerType &lhs,
                                                    const NamedPointerType &rhs) = default;

    [[nodiscard]] friend constexpr NamedPointerType operator+(const NamedPointerType &lhs,
                                                              const SizeType         &rhs)
    {
        return NamedPointerType{lhs._val + rhs.get()};
    }

    [[nodiscard]] friend constexpr NamedPointerType operator-(const NamedPointerType &lhs,
                                                              const SizeType         &rhs)
    {
        return NamedPointerType{lhs._val - rhs.get()};
    }

    [[nodiscard]] friend constexpr SizeType operator-(const NamedPointerType &lhs,
                                                      const NamedPointerType &rhs)
    {
        return SizeType{lhs._val - rhs.get()};
    }

    friend constexpr NamedPointerType &operator+=(NamedPointerType &lhs, const SizeType &rhs)
    {
        lhs._val += rhs.get();
        return lhs;
    }

    friend constexpr NamedPointerType &operator-=(NamedPointerType &lhs, const SizeType &rhs)
    {
        lhs._val -= rhs.get();
        return lhs;
    }
};


/*****************************************************************************
 * NamedNumericType is a strongly typed numeric class.
 *
 * The NamedNumericType is meant to be a safe class which implements a numeric type.
 * This type can be used for generic addition or subtraction with instances of the same type,
 * but not with other types.
 *
 * It is recommended that you make an alias of this class with your parameters. e.g.
 * ~~~~~{.cpp}
 * using SectorCount = detail::NamedNumericType<detail::count_t, struct SectorCountTag>;
 * ~~~~~
 *
 * @param T The storage type.
 * @param Tag Tag to make the type unique and prevent conversions.
 ****************************************************************************/
template <typename T, typename Tag>
class NamedNumericType
{
public:
    using base_type = T;

    /*****************************************************************************
     * Create a new instance of the NamedPointerType with the specified value
     *
     * @param val the initial value
     ****************************************************************************/
    constexpr explicit NamedNumericType(const T &val = {}) : _val(val) {}

    /*****************************************************************************
     * Return the current value in the underlying type.
     *
     * @return The current value in the underlying type.
     ****************************************************************************/
    [[nodiscard]] constexpr const T &get() const { return _val; }

    constexpr NamedNumericType &operator++()
    {
        ++_val;
        return *this;
    }

    constexpr NamedNumericType operator++(int)
    {
        auto self = *this;
        ++_val;
        return self;
    }

    constexpr NamedNumericType &operator--()
    {
        --_val;
        return *this;
    }

    constexpr NamedNumericType operator--(int)
    {
        auto self = *this;
        --_val;
        return self;
    }

private:
    T _val; //< The stored value.

    [[nodiscard]] friend constexpr auto operator<=>(const NamedNumericType &lhs,
                                                    const NamedNumericType &rhs) = default;

    [[nodiscard]] friend constexpr NamedNumericType operator+(const NamedNumericType &lhs,
                                                              const NamedNumericType &rhs)
    {
        return NamedNumericType{lhs._val + rhs._val};
    }

    [[nodiscard]] friend constexpr NamedNumericType operator-(const NamedNumericType &lhs,
                                                              const NamedNumericType &rhs)
    {
        return NamedNumericType{lhs._val - rhs._val};
    }

    friend constexpr NamedNumericType &operator+=(NamedNumericType       &lhs,
                                                  const NamedNumericType &rhs)
    {
        lhs._val += rhs._val;
        return lhs;
    }

    friend constexpr NamedNumericType &operator-=(NamedNumericType       &lhs,
                                                  const NamedNumericType &rhs)
    {
        lhs._val -= rhs._val;
        return lhs;
    }
};

template <typename T, typename SizeType, typename Tag>
std::ostream &operator<<(std::ostream &os, NamedPointerType<T, SizeType, Tag> const &value)
{
    os << "0x" << std::hex << value.get();
    return os;
}

template <typename T, typename Tag>
std::ostream &operator<<(std::ostream &os, NamedNumericType<T, Tag> const &value)
{
    os << value.get();
    return os;
}

} // namespace rj
