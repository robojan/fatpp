/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * A small vector implementation.
 ****************************************************************************/
#pragma once

#include <cstddef>
#include <initializer_list>
#include <compare>
#include <algorithm>
#include <cassert>

namespace rj
{

/*****************************************************************************
 * SmallVector is a fixed capacity vector class. The elements are stored contiguously.
 *
 *
 ****************************************************************************/
template <typename T, std::size_t Capacity>
class SmallVector
{
public:
    using value_type      = T;
    using size_type       = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference       = value_type &;
    using const_reference = const value_type &;
    using iterator        = T *;
    using const_iterator  = const T *;

    /*****************************************************************************
     * Default constructor. Constructs an empty container.
     ****************************************************************************/
    constexpr SmallVector() noexcept = default;

    /*****************************************************************************
     * Constructs the container with count copies of elements with value value.
     *
     * @param count The size of the container.
     * @param value The value to initialize elements of the container with.
     ****************************************************************************/
    constexpr SmallVector(size_type count, const T &value)
    {
        assert(count <= Capacity);
        std::uninitialized_fill_n(begin(), count, value);
        _size = count;
    }

    /*****************************************************************************
     * Constructs the container with count default inserted instances of T.
     *
     * @param count The size of the container.
     ****************************************************************************/
    constexpr explicit SmallVector(size_type count)
    {
        assert(count <= Capacity);
        std::uninitialized_value_construct_n(begin(), count);
        _size = count;
    }

    /*****************************************************************************
     * Constructs the container with the contents of the range [first, last)
     *
     * @param first Range to copy the elements from
     * @param last Range to copy the elements from
     ****************************************************************************/
    template <class InputIt>
    constexpr SmallVector(InputIt first, InputIt last) requires std::input_iterator<InputIt>
    {
        _size = std::distance(first, last);
        assert(_size <= Capacity);
        std::uninitialized_copy(first, last, begin());
    }

    /*****************************************************************************
     * Copy constructor. Constructs the container with the copy of the contents of other.
     *
     * @param other Another container to be used as source to initialize the elements of the
     *      container with.
     ****************************************************************************/
    template <size_type OtherCapacity>
    constexpr SmallVector(const SmallVector<T, OtherCapacity> &other)
    {
        assert(other.size() <= Capacity);
        _size = other.size();
        std::uninitialized_copy(other.begin(), other.end(), begin());
    }

    /*****************************************************************************
     * Move constructor. Constructs the container with the contents of other.
     *
     * @param other Another container to be used as source to initialize the elements of the
     *      container with.
     ****************************************************************************/
    template <size_type OtherCapacity>
    constexpr SmallVector(SmallVector<T, OtherCapacity> &&other) noexcept
    {
        assert(other.size() <= Capacity);
        _size = other.size();
        std::uninitialized_move(other.begin(), other.end(), begin());
        other.clear();
    }

    /*****************************************************************************
     * Constructs the container with the contents of athe initializer list init.
     *
     * @param init Initializer list to initialize the elements of the container with
     ****************************************************************************/
    constexpr SmallVector(std::initializer_list<T> init)
    {
        assert(init.size() <= Capacity);
        _size = init.size();
        std::uninitialized_move(init.begin(), init.end(), begin());
    }

    /*****************************************************************************
     * Destructs the vector.
     ****************************************************************************/
    constexpr ~SmallVector()
    {
        std::destroy_n(begin(), _size);
        _size = 0;
    }

    /*****************************************************************************
     * Replaces the content of the container.
     ****************************************************************************/
    template <size_type OtherCapacity>
    constexpr SmallVector &operator=(const SmallVector<T, OtherCapacity> &other)
    {
        assert(other.size() <= Capacity);
        std::destroy_n(begin(), _size);
        _size = other.size();
        std::uninitialized_copy(other.begin(), other.end(), begin());
        return *this;
    }

    template <size_type OtherCapacity>
    constexpr SmallVector &operator=(SmallVector<T, OtherCapacity> &&other) noexcept
    {
        assert(other.size() <= Capacity);
        std::destroy_n(begin(), _size);
        _size = other.size();
        std::uninitialized_move(other.begin(), other.end(), begin());

        other.clear();
        return *this;
    }

    constexpr SmallVector &operator=(std::initializer_list<T> ilist)
    {
        assert(ilist.size() <= Capacity);
        std::destroy_n(begin(), _size);
        _size = ilist.size();
        std::uninitialized_move(ilist.begin(), ilist.end(), begin());
        return *this;
    }

    /*****************************************************************************
     * Replaces the contents of the container.
     ****************************************************************************/
    constexpr void assign(size_type count, const T &value)
    {
        assert(count <= Capacity);
        std::destroy_n(begin(), _size);
        _size = count;
        std::uninitialized_fill_n(begin(), count, value);
    }

    template <class InputIt>
    constexpr void assign(InputIt first, InputIt last) requires std::input_iterator<InputIt>
    {
        std::destroy_n(begin(), _size);
        _size = std::distance(first, last);
        assert(_size <= Capacity);
        std::uninitialized_move(first, last, begin());
    }

    constexpr void assign(std::initializer_list<T> ilist)
    {
        std::destroy_n(begin(), _size);
        _size = ilist.size();
        assert(_size <= Capacity);
        std::uninitialized_move(ilist.begin(), ilist.end(), begin());
    }

    /*****************************************************************************
     * Returns a reference to the element at the specified location pos.
     ****************************************************************************/
    constexpr reference at(size_type pos)
    {
        assert(pos < size());
        return data()[pos];
    }

    constexpr const_reference at(size_type pos) const
    {
        assert(pos < size());
        return data()[pos];
    }

    constexpr reference operator[](size_type pos)
    {
        assert(pos < size());
        return data()[pos];
    }

    constexpr const_reference operator[](size_type pos) const
    {
        assert(pos < size());
        return data()[pos];
    }

    /*****************************************************************************
     * Returns a reference to first element in the container.
     ****************************************************************************/
    constexpr reference front()
    {
        assert(size() > 0);
        return data()[0];
    }

    constexpr const_reference front() const
    {
        assert(size() > 0);
        return data()[0];
    }

    /*****************************************************************************
     * Returns a reference to last element in the container.
     ****************************************************************************/
    constexpr reference back()
    {
        assert(size() > 0);
        return data()[size() - 1];
    }

    constexpr const_reference back() const
    {
        assert(size() > 0);
        return data()[size() - 1];
    }

    /*****************************************************************************
     * Returns a pointer to the underlying element storage.
     ****************************************************************************/
    constexpr T       *data() noexcept { return reinterpret_cast<T *>(&_buf); }
    constexpr const T *data() const noexcept { return reinterpret_cast<const T *>(&_buf); }

    /*****************************************************************************
     * Returns an iterator to the beginning.
     ****************************************************************************/
    constexpr iterator       begin() noexcept { return data(); }
    constexpr const_iterator begin() const noexcept { return data(); }
    constexpr const_iterator cbegin() const noexcept { return data(); }

    /*****************************************************************************
     * Returns an iterator to the end.
     ****************************************************************************/
    constexpr iterator       end() noexcept { return data() + size(); }
    constexpr const_iterator end() const noexcept { return data() + size(); }
    constexpr const_iterator cend() const noexcept { return data() + size(); }

    /*****************************************************************************
     * Checks whether the container is empty.
     ****************************************************************************/
    [[nodiscard]] constexpr bool empty() const noexcept { return size() == 0; }

    /*****************************************************************************
     * Return the number of elements.
     ****************************************************************************/
    constexpr size_type size() const noexcept { return _size; }

    /*****************************************************************************
     * Return the maximum possible number of elements.
     ****************************************************************************/
    constexpr size_type max_size() const noexcept { return Capacity; }

    /*****************************************************************************
     * Return the maximum possible number of elements.
     ****************************************************************************/
    constexpr size_type capacity() const noexcept { return Capacity; }

    /*****************************************************************************
     * Clears the contents.
     ****************************************************************************/
    constexpr void clear() noexcept
    {
        std::destroy_n(begin(), _size);
        _size = 0;
    }

    /*****************************************************************************
     * Insert elements into the container.
     ****************************************************************************/
    constexpr iterator insert(const_iterator pos, const T &value)
    {
        assert(size() < Capacity);
        auto dest = const_cast<iterator>(pos);
        // Make space in the vector
        std::move_backward(pos, cend(), end() + 1);
        // Add the new element
        std::construct_at(dest, value);
        ++_size;
        return dest;
    }

    constexpr iterator insert(const_iterator pos, T &&value)
    {
        assert(size() < Capacity);
        auto dest = const_cast<iterator>(pos);
        // Make space in the vector
        std::move_backward(pos, cend(), end() + 1);
        // Add the new element
        std::construct_at(dest, std::move(value));
        ++_size;
        return dest;
    }

    constexpr iterator insert(const_iterator pos, size_type count, const T &value)
    {
        assert(size() + count <= Capacity);
        auto dest = const_cast<iterator>(pos);
        // Make space in the vector
        std::move_backward(pos, cend(), end() + count);
        // Add the new element
        std::uninitialized_fill_n(dest, count, value);
        _size += count;
        return dest;
    }

    template <class InputIt>
    constexpr iterator insert(const_iterator pos,
                              InputIt        first,
                              InputIt        last) requires std::input_iterator<InputIt>
    {
        auto count = std::distance(first, last);
        assert(size() + count <= Capacity);
        auto dest = const_cast<iterator>(pos);
        // Make space in the vector
        std::move_backward(pos, cend(), end() + count);
        // Add the new element
        std::uninitialized_move(first, last, dest);
        _size += count;
        return dest;
    }

    constexpr iterator insert(const_iterator pos, std::initializer_list<T> ilist)
    {
        assert(size() + ilist.size() <= Capacity);
        auto dest = const_cast<iterator>(pos);
        // Make space in the vector
        std::move_backward(pos, cend(), end() + ilist.size());
        // Add the new element
        std::uninitialized_move(ilist.begin(), ilist.end(), dest);
        _size += ilist.size();
        return dest;
    }

    template <class... Args>
    constexpr iterator emplace(const_iterator pos, Args &&...args)
    {
        assert(size() < Capacity);
        auto dest = const_cast<iterator>(pos);
        // Make space in the vector
        std::move_backward(pos, cend(), end() + 1);
        // Add the new element
        std::construct_at(dest, std::forward<Args>(args)...);
        ++_size;
        return dest;
    }

    /*****************************************************************************
     * Erases elements.
     ****************************************************************************/
    constexpr iterator erase(const_iterator pos)
    {
        assert(!empty());
        // Remove the element
        auto dest = const_cast<iterator>(pos);
        std::destroy_at(dest);
        std::move(pos + 1, cend(), dest);
        --_size;
        return dest;
    }

    constexpr iterator erase(const_iterator first, const_iterator last)
    {
        auto count = std::distance(first, last);
        assert(size() >= count);
        auto dest = const_cast<iterator>(first);
        // Remove the elements
        std::destroy(first, last);
        std::move(last, cend(), dest);
        _size -= count;
        return dest;
    }

    /*****************************************************************************
     * Adds an element to the end.
     ****************************************************************************/
    constexpr void push_back(const T &value) { insert(end(), value); }
    constexpr void push_back(T &&value) { insert(end(), std::move(value)); }

    /*****************************************************************************
     * Constructs an element in-place at the end.
     ****************************************************************************/
    template <class... Args>
    constexpr reference emplace_back(Args &&...args)
    {
        emplace(end(), std::forward<Args>(args)...);
        return back();
    }

    /*****************************************************************************
     * Removes the last element.
     ****************************************************************************/
    constexpr void pop_back()
    {
        assert(!empty());
        std::destroy_at(end() - 1);
        _size--;
    }

    /*****************************************************************************
     * Changes the number of elements stored.
     ****************************************************************************/
    constexpr void resize(size_type count)
    {
        assert(count <= Capacity);
        if(count > _size)
        {
            // Increase the size
            std::uninitialized_value_construct_n(end(), count - _size);
            _size = count;
        }
        else
        {
            // Decrease the size
            auto diff = _size - count;
            erase(end() - diff, end());
        }
    }

    constexpr void resize(size_type count, const value_type &value)
    {
        assert(count <= Capacity);
        if(count > _size)
        {
            // Increase the size
            std::uninitialized_fill_n(end(), count - _size, value);
            _size = count;
        }
        else
        {
            // Decrease the size
            auto diff = _size - count;
            erase(end() - diff, end());
        }
    }

    /*****************************************************************************
     * Swaps the contents.
     ****************************************************************************/
    constexpr void swap(SmallVector &other) noexcept
    {
        if(this != std::addressof(other))
        {
            if(size() > other.size())
            {
                std::swap_ranges(other.begin(), other.end(), begin());
                std::uninitialized_move(begin() + other.size(), end(), other.end());
                std::swap(_size, other._size);
            }
            else
            {
                std::swap_ranges(begin(), end(), other.begin());
                std::uninitialized_move(other.begin() + size(), other.end(), end());
                std::swap(_size, other._size);
            }
        }
    }

private:
    alignas(alignof(T)) std::byte _buf[sizeof(T) * Capacity];
    size_type _size = 0;
};

template <class T, std::size_t CapacityLhs, std::size_t CapacityRhs>
constexpr bool operator==(const SmallVector<T, CapacityLhs> &lhs,
                          const SmallVector<T, CapacityRhs> &rhs)
{
    if(lhs.size() != rhs.size())
        return false;
    return std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class T, std::size_t CapacityLhs, std::size_t CapacityRhs>
constexpr auto operator<=>(const SmallVector<T, CapacityLhs> &lhs,
                           const SmallVector<T, CapacityRhs> &rhs)
{
    return std::lexicographical_compare_three_way(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

} // namespace rj