/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains random tools.
 ****************************************************************************/
#pragma once

#include <concepts>

#define FATPP_DELETE_COPY(T) \
    T(const T &) = delete;   \
    void operator=(const T &) = delete;

#define FATPP_DELETE_MOVE(T) \
    T(T &&)      = delete;   \
    void operator=(T &&) = delete;

#define FATPP_DELETE_COPY_MOVE(T) \
    FATPP_DELETE_COPY(T)          \
    FATPP_DELETE_MOVE(T)

namespace rj
{
/*****************************************************************************
 * isPowerOf2 returns true if the input is a power of 2. e.g. 2, 4, 16
 *
 * @param x Input value
 * @return true if x is a power of 2
 ****************************************************************************/
template <typename T>
[[nodiscard]] inline constexpr bool isPowerOf2(T x) noexcept requires std::integral<T>
{
    return (x & (x - 1)) == 0;
}

/*****************************************************************************
 * isOdd returns true if the input is odd. e.g. 1, 3, 15
 *
 * @param x Input value
 * @return true if x is odd
 ****************************************************************************/
template <typename T>
[[nodiscard]] inline constexpr bool isOdd(T x) noexcept requires std::integral<T>
{
    return (x & 1) != 0;
}

/*****************************************************************************
 * isEven returns true if the input is even. e.g. 0, 2, 14
 *
 * @param x Input value
 * @return true if x is even
 ****************************************************************************/
template <typename T>
[[nodiscard]] inline constexpr bool isEven(T x) noexcept requires std::integral<T>
{
    return (x & 1) == 0;
}

} // namespace rj
