/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * This file contains compiler specific constants, macros and functions
 *
 ****************************************************************************/
#pragma once

#include <concepts>
#include <cstddef>

namespace rj
{
template <typename T>
constexpr bool is_ValidByteswapSize = sizeof(T) == 1 || sizeof(T) == 2 || sizeof(T) == 4 ||
                                      sizeof(T) == 8;

#if defined(__clang__)
// LLVM Clang compiler
#define FATPP_PACKED_STRUCT_BEGIN struct __attribute__((packed, aligned(4)))
#define FATPP_PACKED_STRUCT_END

template <typename T>
    inline static constexpr T byteswap(T x) noexcept
    requires(std::integral<T> || std::floating_point<T>) &&
    is_ValidByteswapSize<T>
{
    switch(sizeof(T))
    {
    case 1:
        return x;
    case 2:
        return __builtin_bswap16(x);
    case 4:
        return __builtin_bswap32(x);
    case 8:
        return __builtin_bswap64(x);
    }
}

#elif defined(_MSC_VER)
// Microsoft compiler
#define FATPP_PACKED_STRUCT_BEGIN _Pragma("pack(push,1)") struct alignas(4)
#define FATPP_PACKED_STRUCT_END   _Pragma("pack(pop)")

template <typename T>
    inline static constexpr T byteswap(T x) noexcept
    requires(std::integral<T> || std::floating_point<T>) &&
    is_ValidByteswapSize<T>
{
    switch(sizeof(T))
    {
    case 1:
        return x;
    case 2:
        return _byteswap_ushort(x);
    case 4:
        return _byteswap_ulong(x);
    case 8:
        return _byteswap_uint64(x);
    }
}

#elif defined(__GNUC__)
// GCC Compiler
#define FATPP_PACKED_STRUCT_BEGIN struct [[gnu::packed, gnu::aligned(4)]]
#define FATPP_PACKED_STRUCT_END

template <typename T>
    inline static constexpr T byteswap(T x) noexcept
    requires(std::integral<T> || std::floating_point<T>) &&
    is_ValidByteswapSize<T>
{
    switch(sizeof(T))
    {
    case 1:
        return x;
    case 2:
        return __builtin_bswap16(x);
    case 4:
        return __builtin_bswap32(x);
    case 8:
        return __builtin_bswap64(x);
    }
}

#else
#error "Unsupported compiler"
#endif

// Check if the packed struct works
namespace test
{
FATPP_PACKED_STRUCT_BEGIN Foo
{
    uint8_t  a;
    uint32_t b;
    uint16_t c;
};
FATPP_PACKED_STRUCT_END
static_assert(offsetof(Foo, a) == 0);
static_assert(offsetof(Foo, b) == 1);
static_assert(offsetof(Foo, c) == 5);
} // namespace test

} // namespace rj