/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains the definition of the disk interface.
 ****************************************************************************/
#pragma once

#include <cstdint>
#include <span>
#include <util/util.hpp>
#include <fatpp/result.hpp>
#include <fatpp/detail/types.hpp>
#include <fatpp/sector.hpp>
#include <memory>

namespace fatpp
{

/*****************************************************************************
 * The IDisk class is the interface which the hardware layer can implement to
 * interact with the physical disk.
 ****************************************************************************/
class IDisk
{
    FATPP_DELETE_COPY_MOVE(IDisk)
public:
    IDisk() = default;

    /*****************************************************************************
     * Get a buffer from the system driver. This buffer should be uninitialized.
     *
     * @return An uninitialized buffer or error on failure.
     *      The sector object must have a custom deleter that checks if the data has been
     *      modified. If so, write it to the disk.
     ****************************************************************************/
    virtual std::shared_ptr<Sector> getBuffer() = 0;

    /*****************************************************************************
     * Write data to the disk.
     *
     * @param data The sector to be written to the disk. The size of the data will be
     *      in multiples of the sector size. The sector should contain a valid address.
     * @return An error code indicating the status of the operation.
     ****************************************************************************/
    virtual SimpleResult write(std::shared_ptr<Sector> &data) = 0;

    /*****************************************************************************
     * Read data from the disk.
     *
     * @param addr The sector address where the data should be read from off the disk.
     * @return A sector object with the sector data contained within it, or an error code.
     *      The sector object must have a custom deleter that checks if the data has been
     *      modified. If so, write it to the disk.
     ****************************************************************************/
    virtual Result<std::shared_ptr<Sector>> read(SectorAddress addr) = 0;

    /*****************************************************************************
     * Return the size of the disk in sectors.
     *
     * @return The size of the disk in sectors.
     ****************************************************************************/
    virtual SectorCount size() const = 0;

    /*****************************************************************************
     * Mark data on the disk as deleted. This function is optional.
     *
     * @param addr The address on which to mark the data as deleted.
     * @param count The number of sectors to mark as deleted.
     * @return An error code indicating the status of the operation.
     ****************************************************************************/
    virtual SimpleResult trim(SectorAddress addr, SectorCount count)
    {
        return ErrorCode::NotImplemented;
    };

    /*****************************************************************************
     * Get the current date and time. This is used to store the file modification
     * dates. This function is optional.
     *
     * @return The current date and time, or the status code.
     ****************************************************************************/
    virtual SimpleResult dateTime() const { return ErrorCode::NotImplemented; };
};

} // namespace fatpp
