/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains the file system state class
 ****************************************************************************/
#pragma once

#include <fatpp/detail/types.hpp>
#include <fatpp/disk.hpp>
#include <fatpp/config.hpp>
#include <vector>
#include <util/smallvector.hpp>

namespace fatpp::detail
{

struct State
{
    IDisk &disk; //< Hardware interface object
    rj::SmallVector<SectorAddress, fatpp::config::kMaxNumFat>
                  fatLocations;      //< Array of pointers to FAT locations
    SectorCount   fatSize;           //< Size of the FAT
    SectorCount   sectorsPerCluster; //< Number of sectors per cluster
    SectorAddress firstDataSector;   //< Sector on which the data starts
    DirEntryCount rootDirEntries;    //< Number of entries in the root directory
    uint8_t       sectorSize; //< The size of the sector in the power of to. e.g. 9 is 512 bytes, 10
    // is 1kB
    unsigned sectorMask; //< The mask to get the byte address in the sector. e.g. sectorsize 9 will
    // be 0x1FF
    ClusterCount   fsSize;          //< The size of the FAT filesystem in number of clusters.
    ClusterAddress lastFreeCluster; //< The address of the last known free cluster.
};

} // namespace fatpp::detail
