/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains the implementation of the FAT handling
 ****************************************************************************/
#pragma once

#include <fatpp/detail/state.hpp>
#include <fatpp/result.hpp>

namespace fatpp::detail
{

class FatIterator;

class IFat
{
public:
    IFat(State &state, ClusterAddress badCluster, ClusterAddress eofCluster)
        : kBadCluster(badCluster), kEofCluster(eofCluster), _state(state)
    {
    }

    const ClusterAddress                         kBadCluster;
    const ClusterAddress                         kEofCluster;
    [[nodiscard]] virtual Result<ClusterAddress> getNextCluster(ClusterAddress addr) const = 0;
    [[nodiscard]] virtual Result<ClusterAddress> getFreeCluster() const                    = 0;
    [[nodiscard]] virtual SimpleResult setCluster(ClusterAddress addr, ClusterAddress val) = 0;

    FatIterator getChain(ClusterAddress addr) const;

protected:
    State &_state;

    Result<std::shared_ptr<Sector>> getFatSector(SectorCount offset) const;
};

class Fat12 : public IFat
{
public:
    Fat12(State &state) : IFat(state, ClusterAddress{0xFF7}, ClusterAddress{0xFF8}) {}

    [[nodiscard]] Result<ClusterAddress> getNextCluster(ClusterAddress addr) const override;
    [[nodiscard]] Result<ClusterAddress> getFreeCluster() const override;
    [[nodiscard]] SimpleResult setCluster(ClusterAddress addr, ClusterAddress val) override;

private:
    unsigned clusterToFatOffset(ClusterAddress addr) const { return addr.get() + addr.get() / 2; }
    unsigned clusterToFatOffset(ClusterCount addr) const { return addr.get() + addr.get() / 2; }
};

class Fat16 : public IFat
{
public:
    Fat16(State &state) : IFat(state, ClusterAddress{0xFFF7}, ClusterAddress{0xFFF8}) {}

    [[nodiscard]] Result<ClusterAddress> getNextCluster(ClusterAddress addr) const override;
    [[nodiscard]] Result<ClusterAddress> getFreeCluster() const override;
    [[nodiscard]] SimpleResult setCluster(ClusterAddress addr, ClusterAddress val) override;
};

class Fat32 : public IFat
{
public:
    static constexpr uint32_t kFat32Mask = 0x0FFFFFFFU;
    Fat32(State &state) : IFat(state, ClusterAddress{0x0FFFFFF7}, ClusterAddress{0x0FFFFFF8}) {}

    [[nodiscard]] Result<ClusterAddress> getNextCluster(ClusterAddress addr) const override;
    [[nodiscard]] Result<ClusterAddress> getFreeCluster() const override;
    [[nodiscard]] SimpleResult setCluster(ClusterAddress addr, ClusterAddress val) override;
};

class FatIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using difference_type   = ClusterCount;
    using value_type        = ClusterAddress;
    using pointer           = ClusterAddress *;
    using reference         = ClusterAddress &;

    FatIterator(const IFat &fat, ClusterAddress initialCluster) : _fat(fat), _ptr(initialCluster) {}

    FatIterator &operator++()
    {
        auto result = _fat.getNextCluster(_ptr);
        _error      = result.status();
        _ptr        = result ? result.result() : _fat.kEofCluster;
        return *this;
    }
    FatIterator operator++(int)
    {
        auto self   = *this;
        auto result = _fat.getNextCluster(_ptr);
        _error      = result.status();
        _ptr        = result ? result.result() : _fat.kEofCluster;
        return self;
    }

    [[nodiscard]] bool operator==(const FatIterator &other) const { return _ptr == other._ptr; }
    [[nodiscard]] bool operator!=(const FatIterator &other) const { return _ptr != other._ptr; }
    [[nodiscard]]      operator bool() const { return _error == ErrorCode::Ok && !isLast(); }
    [[nodiscard]] ErrorCode status() const { return _error; }

    [[nodiscard]] bool isLast() const { return _ptr >= _fat.kEofCluster; }

    [[nodiscard]] ClusterAddress        operator*() const { return _ptr; }
    [[nodiscard]] const ClusterAddress *operator->() const { return &_ptr; }

private:
    const IFat    &_fat;
    ClusterAddress _ptr;
    ErrorCode      _error = ErrorCode::Ok;
};

} // namespace fatpp::detail
