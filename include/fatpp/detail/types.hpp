/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains standard types used in the fatpp library.
 ****************************************************************************/
#pragma once

#include <cstdint>
#include <util/namedtype.hpp>

namespace fatpp
{
namespace detail
{
/*****************************************************************************
 * Empty struct with no elements.
 ****************************************************************************/
struct Empty
{
};

using addr_t  = uint32_t;
using count_t = uint32_t;

} // namespace detail

using SectorCount   = rj::NamedNumericType<detail::count_t, struct SectorCountTag>;
using ClusterCount  = rj::NamedNumericType<detail::count_t, struct ClusterCountTag>;
using DirEntryCount = rj::NamedNumericType<detail::count_t, struct DirEntryCountTag>;

using SectorAddress  = rj::NamedPointerType<detail::addr_t, SectorCount, struct SectorAddressTag>;
using ClusterAddress = rj::NamedPointerType<detail::addr_t, ClusterCount, struct ClusterAddressTag>;


}; // namespace fatpp
