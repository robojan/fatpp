/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * This file contains the structure definiations of the FAT file system
 *
 ****************************************************************************/
#pragma once

#include <cstdint>
#include <array>
#include <string_view>
#include <util/compiler.hpp>
#include <util/endian.hpp>
#include <fatpp/literal.hpp>

namespace fatpp::structure
{
using namespace fatpp::literals;

// NOLINTBEGIN
FATPP_PACKED_STRUCT_BEGIN BiosParameterBlock
{
    std::array<uint8_t, 3> jmpBoot;
    std::array<char, 8>    oemName;
    uint16_t               bytsPerSec;
    uint8_t                secPerClus;
    uint16_t               rsvdSecCnt;
    uint8_t                numFATs;
    uint16_t               rootEntCnt;
    uint16_t               totSec16;
    uint8_t                media;
    uint16_t               fatSz16;
    uint16_t               secPerTrk;
    uint16_t               numHeads;
    uint32_t               hiddSec;
    uint32_t               totSec32;

    // String accessor function
    [[nodiscard]] constexpr std::string_view oemNameStr() const
    {
        return std::string_view(oemName.data(), sizeof(oemName));
    }

    // Default values
    static constexpr std::array<uint8_t, 3> c_jmpBootValue{0xeb, 0x00, 0x90}; // x86 jmp instruction
    static constexpr std::array<char, 8>    c_oemNameValue{'M', 'S', 'W', 'I', 'N', '4', '.', '1'};
};
FATPP_PACKED_STRUCT_END
static_assert(sizeof(BiosParameterBlock) == 36);

FATPP_PACKED_STRUCT_BEGIN BiosParameterBlockFat12 : public BiosParameterBlock
{
    uint8_t  drvNum;
    uint8_t  reserved1;
    uint8_t  bootSig;
    uint32_t volId;
    char     volLab[11];
    char     filSysType[8];
    uint8_t  filler[448];
    uint16_t signature;

    // String accessor function
    [[nodiscard]] constexpr std::string_view volLabStr() const
    {
        return std::string_view(volLab, sizeof(volLab));
    }
    [[nodiscard]] constexpr std::string_view filSysTypeStr() const
    {
        return std::string_view(filSysType, sizeof(filSysType));
    }

    // signature value
    static constexpr uint16_t c_sigValue     = 0xaa55_fatu16;
    static constexpr uint16_t c_bootSigValue = 0x29_fatu8;
};
FATPP_PACKED_STRUCT_END
static_assert(sizeof(BiosParameterBlockFat12) == 512);

// Fat 16 uses the same BIOS parameter block as FAT12
using BiosParameterBlockFat16 = BiosParameterBlockFat12;

FATPP_PACKED_STRUCT_BEGIN BiosParameterBlockFat32 : public BiosParameterBlock
{
    uint32_t fatSz32;
    struct
    {
        uint16_t activeFat : 4;
        uint16_t reserved1 : 3;
        uint16_t singleFat : 1;
        uint16_t reserved2 : 8;
    } extFlags;
    uint16_t fsVer;
    uint32_t rootClus;
    uint16_t fsInfo;
    uint16_t bkBootSec;
    uint8_t  reserved1[12];
    uint8_t  drvNum;
    uint8_t  reserved2;
    uint8_t  bootSig;
    uint32_t volId;
    char     volLab[11];
    char     filSysType[8];
    uint8_t  filler[420];
    uint16_t signature;

    // String accessor function
    [[nodiscard]] constexpr std::string_view volLabStr() const
    {
        return std::string_view(volLab, sizeof(volLab));
    }
    [[nodiscard]] constexpr std::string_view filSysTypeStr() const
    {
        return std::string_view(filSysType, sizeof(filSysType));
    }

    // signature value
    static constexpr uint16_t c_sigValue     = 0xaa55_fatu16;
    static constexpr uint16_t c_bootSigValue = 0x29_fatu8;
};
FATPP_PACKED_STRUCT_END
static_assert(sizeof(BiosParameterBlockFat32) == 512);

// NOLINTEND
} // namespace fatpp::structure