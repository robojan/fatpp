/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * This file contains the implementation of the filesystem object. The
 * filesystem object is the general entry point of the filesystem.
 *****************************************************************************/

#pragma once

#include <cstdint>
#include <fatpp/detail/structure.hpp>
#include <fatpp/detail/state.hpp>

namespace fatpp
{
/*****************************************************************************
 * List op supported filesystem types.
 ****************************************************************************/
enum FileSystemType
{
    UnknownFsType, //< Unknown filesystem type
    FAT12,
    FAT16,
    FAT32,
};

/*****************************************************************************
 * The filesystem class is the general entrypoint to the filesystem.
 *
 * This class is the entry point to do filesystem operations. For example
 * deleting, opening, renaming or moving a file. This class must be created
 * by one of the factory functions.
 ****************************************************************************/
class FileSystem
{
public:
private:
    explicit FileSystem(const structure::BiosParameterBlock &bpb);
};

namespace detail
{
/*****************************************************************************
 * getTotalSectors returns the total number of sectors in the FAT filesystem
 *
 * @param bpb The bios parameter block get the size from
 * @return The number of sectors in the filesystem.
 ****************************************************************************/
[[nodiscard]] inline uint32_t getTotalSectors(const structure::BiosParameterBlock &bpb)
{
    return bpb.totSec16 == 0 ? bpb.totSec32 : bpb.totSec16;
}

/*****************************************************************************
 * getFatSize returns the size of one FAT.
 *
 * @param bpb The bios parameter block get the size from
 * @return The number of sectors that one FAT would consist off.
 ****************************************************************************/
[[nodiscard]] inline uint32_t getFatSize(const structure::BiosParameterBlock &bpb)
{
    return bpb.fatSz16;
}

/*****************************************************************************
 * getFatSize returns the size of one FAT.
 *
 * @param bpb The bios parameter block get the size from
 * @return The number of sectors that one FAT would consist off.
 ****************************************************************************/
[[nodiscard]] inline uint32_t getFatSize(const structure::BiosParameterBlockFat32 &bpb)
{
    return bpb.fatSz16 == 0 ? bpb.fatSz32 : bpb.fatSz16;
}

/*****************************************************************************
 * isValidBpb will return if the bios parameter block contains valid entries.
 *
 * @param bpb The bios parameter block to check
 * @param diskSize The disk size to compare the BPB against.
 *      If 0 this check is ignored.
 * @return True if the bios parameter block is valid.
 ****************************************************************************/
[[nodiscard]] bool isValidBpb(const structure::BiosParameterBlock &bpb, uint32_t diskSize = 0);

/*****************************************************************************
 * getFatType will return the automatically detected FAT filesystem type from
 * the bios parameter block.
 *
 * It will determine the FAT type based on the number of clusters. As per the
 * FAT specification.
 *
 * @param bpb The bios parameter block, from which to determine the filesystem
 *      type.
 * @return The detected filesystem type. UnknownFsType will be returned when
 *      the type cannot be determined.
 ****************************************************************************/
[[nodiscard]] FileSystemType getFatType(const structure::BiosParameterBlock &bpb);

} // namespace detail

}; // namespace fatpp
