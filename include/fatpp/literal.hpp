/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * This file will define user defined literals.
 ****************************************************************************/
#pragma once

#include <limits>
#include <cassert>
#include <util/compiler.hpp>
#include <util/endian.hpp>

namespace fatpp::literals
{
/*****************************************************************************
 * FAT literals
 *
 * The FAT literals will create numeric constants in the correct endianness
 * for storing on the filesytem.
 * You can use these by appending _fatu16 to a literal for example, to get a
 * 16 bit unsigned integer in little endian, independent of the host system.
 *
 ****************************************************************************/
constexpr uint8_t operator""_fatu8(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<uint8_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<uint8_t>(x));
}
static_assert(sizeof(10_fatu8) == 1);

constexpr uint16_t operator""_fatu16(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<uint16_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<uint16_t>(x));
}
static_assert(sizeof(10_fatu16) == 2);

constexpr uint32_t operator""_fatu32(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<uint32_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<uint32_t>(x));
}
static_assert(sizeof(10_fatu32) == 4);

constexpr uint64_t operator""_fatu64(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<uint64_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<uint64_t>(x));
}
static_assert(sizeof(10_fatu64) == 8);

constexpr int8_t operator""_fati8(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<int8_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<int8_t>(x));
}
static_assert(sizeof(10_fati8) == 1);

constexpr int16_t operator""_fati16(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<int16_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<int16_t>(x));
}
static_assert(sizeof(10_fati16) == 2);

constexpr int32_t operator""_fati32(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<int32_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<int32_t>(x));
}
static_assert(sizeof(10_fati32) == 4);

constexpr int64_t operator""_fati64(unsigned long long x) noexcept
{
    using limits = std::numeric_limits<int64_t>;
    assert(x >= limits::min() && x <= limits::max());
    return rj::toLittle(static_cast<int64_t>(x));
}
static_assert(sizeof(10_fati64) == 8);

} // namespace fatpp::literals
