/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains the error codes and the error class.
 ****************************************************************************/
#pragma once

#include <optional>
#include <cassert>
#include <fatpp/detail/types.hpp>

namespace fatpp
{
/*****************************************************************************
 * ErrorCode enum. Contains the list of all valid error codes in use by the fatpp
 * library.
 ****************************************************************************/
enum class ErrorCode
{
    Ok,              //< All is good.
    NoMemory,        //< Memory allocation failed.
    InvalidArgument, //< Argument provided is invalid.
    NotImplemented,  //< Function not implemented.
    NoSpace,         //< No space left.
};

class SimpleResult
{
public:
    constexpr SimpleResult(ErrorCode status = ErrorCode::Ok) : _status(status) {}

    /*****************************************************************************
     * Convert result class to bool to check if the operation was successful
     ****************************************************************************/
    [[nodiscard]] constexpr explicit operator bool() const { return _status == ErrorCode::Ok; }

    /*****************************************************************************
     * Returns the error code of the result.
     *
     * @return The stored error code.
     ****************************************************************************/
    [[nodiscard]] constexpr ErrorCode status() const { return _status; }

private:
    ErrorCode _status;
};

/*****************************************************************************
 * The result class can be used to return results from functions with error
 * codes
 *
 * @param T The type of the result.
 ****************************************************************************/
template <typename T>
class Result : public SimpleResult
{
public:
    /*****************************************************************************
     * Constructor of the Result class.
     *
     * @param result Result value
     ****************************************************************************/

    template <typename U>
    constexpr Result(U &&result) : SimpleResult(ErrorCode::Ok), _result(std::forward<U>(result))
    {
    }

    /*****************************************************************************
     * Constructor of the Result class.
     *
     * @param status Error code result of the function
     ****************************************************************************/
    constexpr Result(ErrorCode status) : SimpleResult(status) { assert(status != ErrorCode::Ok); }

    /*****************************************************************************
     * Get the result value.
     ****************************************************************************/
    [[nodiscard]] constexpr const T &result() const { return *_result; }

    /*****************************************************************************
     * Get the result value.
     ****************************************************************************/
    [[nodiscard]] constexpr T &result() { return *_result; }

    /*****************************************************************************
     * Get the result value.
     ****************************************************************************/
    [[nodiscard]] constexpr const T &operator*() const { return *_result; }

    /*****************************************************************************
     * Get the result value.
     ****************************************************************************/
    [[nodiscard]] constexpr T &operator*() { return *_result; }

    /*****************************************************************************
     * Get the result value.
     ****************************************************************************/
    [[nodiscard]] constexpr const T *operator->() const { return &(*_result); }

    /*****************************************************************************
     * Get the result value.
     ****************************************************************************/
    [[nodiscard]] constexpr T *operator->() { return &(*_result); }

private:
    std::optional<T> _result;
};

} // namespace fatpp
