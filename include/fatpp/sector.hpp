/*
 * Copyright 2021 Robbert-Jan de Jager
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*****************************************************************************
 * This file contains the decleration of the sector object
 ****************************************************************************/
#pragma once

#include <fatpp/detail/types.hpp>

#include <span>
#include <cstddef>
#include <cassert>

#include <util/smallvector.hpp>
#include <fatpp/config.hpp>

namespace fatpp
{

/*****************************************************************************
 * Sector object. This object is a owning sector buffer object.
 * Each sector object contains a buffer which is allocated by the external
 * System interface.
 *
 * It is the responsibility of the user to allocate this object in a shared pointer
 * object which contains a custom deleter, which deletes the sector object
 * and the contained buffer.
 *
 ****************************************************************************/
class Sector
{
public:
    /*****************************************************************************
     * Sector constructor. The buffer will be stored and can be used through the
     * sector interface. This object will be clean and not assigned to an address.
     *
     * @param buffer Pointer of the buffer to store
     * @param size The size of the buffer.
     ****************************************************************************/
    Sector(std::byte *buffer, std::size_t size) : _buffer(buffer), _bufferSize(size) {}

    /*****************************************************************************
     * Sector constructor. The buffer will be stored and can be used through the
     * sector interface. This object will be clean and assigned to an address.
     *
     * @param addr The address to assign this sector to.
     * @param buffer Pointer of the buffer to store
     * @param size The size of the buffer.
     ****************************************************************************/
    Sector(SectorAddress addr, std::byte *buffer, std::size_t size)
        : _addresses({addr}), _buffer(buffer), _bufferSize(size)
    {
    }

    /*****************************************************************************
     * Returns true if the sector is marked as dirty.
     *
     * @return True if the sector is dirty.
     ****************************************************************************/
    bool isDirty() const { return _dirty; }

    /*****************************************************************************
     * Mark the sector as dirty.
     ****************************************************************************/
    void markDirty() { _dirty = true; }

    /*****************************************************************************
     * Mark the sector as not dirty.
     ****************************************************************************/
    void clearDirty() { _dirty = false; }

    /*****************************************************************************
     * Return the address which this sector was assigned to. Undefined if no sector
     * is assigned.
     *
     * @param idx The idx'th address of the sector.
     * @return The assigned sector address.
     ****************************************************************************/
    SectorAddress address(int idx = 0) const { return _addresses[idx]; }

    [[nodiscard]] auto &addresses() const { return _addresses; }

    /*****************************************************************************
     * Returns true if an address has been assigned to the sector.
     *
     * @return True if the sector has an address.
     ****************************************************************************/
    [[nodiscard]] bool hasAddress() const { return !_addresses.empty(); }

    /*****************************************************************************
     * Returns the number of addresses that the sector has.
     *
     * @return The number of addresses.
     ****************************************************************************/
    unsigned numAddresses() const { return _addresses.size(); }

    /*****************************************************************************
     * Set the sector address to the specified value.
     *
     * @param address The address to set.
     ****************************************************************************/
    void addAddress(SectorAddress address) { _addresses.push_back(address); }

    /*****************************************************************************
     * Returns a pointer to the start of the buffer.
     *
     * @return The pointer to the start of the buffer.
     ****************************************************************************/
    std::byte       *begin() { return _buffer; }
    const std::byte *begin() const { return _buffer; }

    /*****************************************************************************
     * Returns a pointer to the end of the buffer.
     *
     * @return The pointer to the end of the buffer.
     ****************************************************************************/
    std::byte       *end() { return _buffer + _bufferSize; }
    const std::byte *end() const { return _buffer + _bufferSize; }

    /*****************************************************************************
     * Returns a pointer to the start of the buffer.
     *
     * @return The pointer to the start of the buffer.
     ****************************************************************************/
    std::byte       *data() { return _buffer; }
    const std::byte *data() const { return _buffer; }

    /*****************************************************************************
     * Returns the size of the buffer.
     *
     * @return The size of the buffer.
     ****************************************************************************/
    std::size_t size() const { return _bufferSize; }

    /*****************************************************************************
     * Return a reference to the idx-th element of the array.
     *
     * @param idx Index into the buffer.
     * @return The idx-th element of the array.
     ****************************************************************************/
    std::byte operator[](std::size_t idx) const
    {
        assert(idx < _bufferSize);
        return _buffer[idx];
    }
    std::byte &operator[](std::size_t idx)
    {
        assert(idx < _bufferSize);
        return _buffer[idx];
    }

private:
    bool _dirty : 1 = false; //< Flag which should be set when the data is modified
    rj::SmallVector<SectorAddress, config::kMaxNumFat>
                _addresses;            //< The addresses of the sector, if the assigned flag is set.
    std::byte  *_buffer     = nullptr; //< Buffer containing the sector data.
    std::size_t _bufferSize = 0;       //< The size of the buffer
};

} // namespace fatpp