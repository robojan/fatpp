FROM ubuntu:21.04
LABEL maintainer=robojan1+gitlabdocker@hotmail.com

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y \
    clang \
    clang-format \
    clang-tidy \
    clang-tools \
    llvm \
    cmake \
    doxygen \
    git \
    graphviz \
    ninja-build \
    && rm -rf /var/lib/apt/lists/*

CMD [ "/bin/bash" ]
WORKDIR /work
