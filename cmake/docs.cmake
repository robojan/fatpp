option(BUILD_DOCS "Build documentation" OFF)

if(BUILD_DOCS)
    find_package(Doxygen REQUIRED dot)
    
    set(DOXYGEN_GENERATE_HTML YES)
    set(DOXYGEN_RECURSIVE YES)
    set(DOXYGEN_EXCLUDE_PATTERNS "*/build/*")
    set(DOXYGEN_JAVADOC_BANNER YES)
    set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
    set(DOXYGEN_JAVADOC_BLOCK YES)
    set(DOXYGEN_BUILTIN_STL_SUPPORT YES)

    doxygen_add_docs(docs ${PROJECT_SOURCE_DIR} COMMENT "Generate documentation")
endif()