# FAT++ - C++ FAT filesystem library {#mainpage}
The FAT++ provides a modern c++ library for interfacing with the FAT filesystem.

# Doxygen documentation
Automatically generated documentation can be found [here](https://robojan.gitlab.io/fatpp/)